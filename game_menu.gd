extends Control

# game_menu.gd

@onready var menu := $"."
@onready var hud := $"../Hud"
@onready var howto := $"../HowToPlay"

var howto_vis = false

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
#	get_tree().paused = true

func _on_play_but_pressed():
	get_tree().paused = false
	# capture mouse
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	menu.visible = false
	hud.visible = true

func _on_set_but_pressed():
	if howto_vis:
		howto.visible = false
		howto_vis = false
	else:
		howto_vis = true
		howto.visible = true

func _on_quit_but_pressed():
	get_tree().quit()

