extends HBoxContainer

# skilltree.gd

@export var experience = 0

@export var survival_skill = 0
@export var healing_skill = 0
@export var farming_skill = 0
@export var shield_skill = 0
@export var medic_skill = 0
@export var gourmet_skill = 0
@export var cooking_skill = 0

@export var fighter_skill = 0
@export var stamina_skill = 0
@export var clubs_skill = 0
@export var gundmg_skill = 0
@export var gunprec_skill = 0
@export var carry_skill = 0
@export var blades_skill = 0

@onready var right_side := $"RightSide/GridContainer"
@onready var label1 := $"RightSide/LabelGridContainer/Label1"
@onready var label2 := $"RightSide/LabelGridContainer/Label2"
@onready var label3 := $"RightSide/LabelGridContainer/Label3"
@onready var label4 := $"RightSide/LabelGridContainer/Label4"

var audio_player = preload("res://sound/audio_player.tscn")
var audio_error = preload("res://sound/error.wav")
var audio_lvl = preload("res://sound/lvlup.wav")

var circle_ico = preload("res://ui/circle.png")
var check_ico = preload("res://ui/check-mark.png")

var exp_req = 200
var player_lvl = 0
var tmp_txt = ""
var skillpoints = 0

func _ready():
	tmp_txt = "Exp. : %d" % experience
	label1.text = tmp_txt
	tmp_txt = "Exp. required: %d" % exp_req
	label2.text = tmp_txt
	tmp_txt = "Level: %d" % player_lvl
	label3.text = tmp_txt
	tmp_txt = "Skillpoints: %d" % skillpoints
	label4.text = tmp_txt

func _play_error():
	var sound = audio_player.instantiate()
	add_child(sound)
	sound.play_sound(audio_error)

func _play_lvl():
	var sound = audio_player.instantiate()
	add_child(sound)
	sound.play_sound(audio_lvl)

func update_exp():
	if experience >= exp_req:
		exp_req = exp_req * 2
		player_lvl += 1
		skillpoints += 1
	tmp_txt = "Exp. : %d" % experience
	label1.text = tmp_txt
	tmp_txt = "Exp. required: %d" % exp_req
	label2.text = tmp_txt
	tmp_txt = "Level: %d" % player_lvl
	label3.text = tmp_txt
	tmp_txt = "Skillpoints: %d" % skillpoints
	label4.text = tmp_txt

#---------- SURVIVAL ----------#

func _on_sur_but_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# Survival 1 - 10
	for n in range(1, 11):
		var sur_but = Button.new()
		sur_but.text = "        Survival level %d        " % n
		sur_but.connect("pressed",Callable(self,"_button_pressed").bind(n))
		right_side.add_child(sur_but)
		var TR = TextureRect.new()
		if survival_skill >= n:
			TR.texture = chk_tex
		else:
			TR.texture = cir_tex
		right_side.add_child(TR)

func _button_pressed(extra_arg_0: int):
	if survival_skill == (extra_arg_0 - 1) and skillpoints > 0:
		survival_skill = extra_arg_0
		skillpoints -= 1
		update_exp()
		_on_sur_but_pressed()
		var sound = audio_player.instantiate()
		add_child(sound)
		_play_lvl()
	else:
		_play_error()

func _on_sur_healing_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# healing 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Health regen %d (req. Survival level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_healing_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _healing_pressed(arg: int):
	var req_met = false
	if arg == 1 and survival_skill >= 2:
		req_met = true
	if arg == 2 and survival_skill >= 4:
		req_met = true
	if arg == 3 and survival_skill >= 6:
		req_met = true
	if healing_skill == (arg - 1) and skillpoints > 0 and req_met:
		healing_skill = arg
		skillpoints -= 1
		update_exp()
		_on_sur_healing_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_sur_farming_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# farming 1 - 5
	for n in range(1, 6):
		var heal_but = Button.new()
		var f_str = "Farming level %d (req. Survival level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_farming_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _farming_pressed(arg: int):
	var req_met = false
	if arg == 1 and survival_skill >= 2:
		req_met = true
	if arg == 2 and survival_skill >= 4:
		req_met = true
	if arg == 3 and survival_skill >= 6:
		req_met = true
	if arg == 4 and survival_skill >= 8:
		req_met = true
	if arg == 5 and survival_skill >= 10:
		req_met = true
	if farming_skill == (arg - 1) and skillpoints > 0 and req_met:
		farming_skill = arg
		skillpoints -= 1
		update_exp()
		_on_sur_farming_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_sur_shield_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# shield 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Damage resist %d (req. Survival level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_shield_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _shield_pressed(arg: int):
	var req_met = false
	if arg == 1 and survival_skill >= 2:
		req_met = true
	if arg == 2 and survival_skill >= 4:
		req_met = true
	if arg == 3 and survival_skill >= 6:
		req_met = true
	if shield_skill == (arg - 1) and skillpoints > 0 and req_met:
		shield_skill = arg
		skillpoints -= 1
		update_exp()
		_on_sur_shield_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_sur_medic_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# medic 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Medic level %d (req. Survival level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_medic_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _medic_pressed(arg: int):
	var req_met = false
	if arg == 1 and survival_skill >= 2:
		req_met = true
	if arg == 2 and survival_skill >= 4:
		req_met = true
	if arg == 3 and survival_skill >= 6:
		req_met = true
	if medic_skill == (arg - 1) and skillpoints > 0 and req_met:
		medic_skill = arg
		skillpoints -= 1
		update_exp()
		_on_sur_medic_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_sur_gourmet_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# gourmet 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Gourmet level %d (req. Survival level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_gourmet_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _gourmet_pressed(arg: int):
	var req_met = false
	if arg == 1 and survival_skill >= 2:
		req_met = true
	if arg == 2 and survival_skill >= 4:
		req_met = true
	if arg == 3 and survival_skill >= 6:
		req_met = true
	if gourmet_skill == (arg - 1) and skillpoints > 0 and req_met:
		gourmet_skill = arg
		skillpoints -= 1
		update_exp()
		_on_sur_gourmet_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_sur_cooking_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# cooking 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Cooking level %d (req. Survival level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_cooking_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _cooking_pressed(arg: int):
	var req_met = false
	if arg == 1 and survival_skill >= 2:
		req_met = true
	if arg == 2 and survival_skill >= 4:
		req_met = true
	if arg == 3 and survival_skill >= 6:
		req_met = true
	if cooking_skill == (arg - 1) and skillpoints > 0 and req_met:
		cooking_skill = arg
		skillpoints -= 1
		update_exp()
		_on_sur_cooking_pressed()
		_play_lvl()
	else:
		_play_error()

#---------- FIGHTER ----------#

func _on_fig_but_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# Survival 1 - 10
	for n in range(1, 11):
		var fig_but = Button.new()
		fig_but.text = "        Fighter level %d        " % n
		fig_but.connect("pressed",Callable(self,"_figbut_pressed").bind(n))
		right_side.add_child(fig_but)
		var TR = TextureRect.new()
		if survival_skill >= n:
			TR.texture = chk_tex
		else:
			TR.texture = cir_tex
		right_side.add_child(TR)

func _figbut_pressed(arg: int):
	if fighter_skill == (arg - 1) and skillpoints > 0:
		fighter_skill = arg
		skillpoints -= 1
		update_exp()
		_on_fig_but_pressed()
		var sound = audio_player.instantiate()
		add_child(sound)
		_play_lvl()
	else:
		_play_error()

func _on_fig_stamina_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# stamina 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Stamina regen %d (req. Fighter level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_stamina_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _stamina_pressed(arg: int):
	var req_met = false
	if arg == 1 and fighter_skill >= 2:
		req_met = true
	if arg == 2 and fighter_skill >= 4:
		req_met = true
	if arg == 3 and fighter_skill >= 6:
		req_met = true
	if stamina_skill == (arg - 1) and skillpoints > 0 and req_met:
		stamina_skill = arg
		skillpoints -= 1
		update_exp()
		_on_fig_stamina_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_fig_clubs_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# stamina 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Clubs and bats %d (req. Fighter level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_clubs_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _clubs_pressed(arg: int):
	var req_met = false
	if arg == 1 and fighter_skill >= 2:
		req_met = true
	if arg == 2 and fighter_skill >= 4:
		req_met = true
	if arg == 3 and fighter_skill >= 6:
		req_met = true
	if clubs_skill == (arg - 1) and skillpoints > 0 and req_met:
		clubs_skill = arg
		skillpoints -= 1
		update_exp()
		_on_fig_clubs_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_fig_gun_dmg_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# stamina 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Gun damage %d (req. Fighter level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_gundmg_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _gundmg_pressed(arg: int):
	var req_met = false
	if arg == 1 and fighter_skill >= 2:
		req_met = true
	if arg == 2 and fighter_skill >= 4:
		req_met = true
	if arg == 3 and fighter_skill >= 6:
		req_met = true
	if gundmg_skill == (arg - 1) and skillpoints > 0 and req_met:
		gundmg_skill = arg
		skillpoints -= 1
		update_exp()
		_on_fig_gun_dmg_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_fig_gun_prec_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# stamina 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Gun prec. %d (req. Fighter level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_gunprec_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _gunprec_pressed(arg: int):
	var req_met = false
	if arg == 1 and fighter_skill >= 2:
		req_met = true
	if arg == 2 and fighter_skill >= 4:
		req_met = true
	if arg == 3 and fighter_skill >= 6:
		req_met = true
	if gunprec_skill == (arg - 1) and skillpoints > 0 and req_met:
		gunprec_skill = arg
		skillpoints -= 1
		update_exp()
		_on_fig_gun_prec_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_fig_strength_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# stamina 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Carry wgt. %d (req. Fighter level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_strength_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _strength_pressed(arg: int):
	var req_met = false
	if arg == 1 and fighter_skill >= 2:
		req_met = true
	if arg == 2 and fighter_skill >= 4:
		req_met = true
	if arg == 3 and fighter_skill >= 6:
		req_met = true
	if carry_skill == (arg - 1) and skillpoints > 0 and req_met:
		carry_skill = arg
		skillpoints -= 1
		update_exp()
		_on_fig_strength_pressed()
		_play_lvl()
	else:
		_play_error()

func _on_fig_blades_pressed():
	# remove_at existing items from grid
	for n in right_side.get_children():
		n.queue_free()
	# icons
	var cir_tex = ImageTexture.new()
	cir_tex = circle_ico
	var chk_tex = ImageTexture.new()
	chk_tex = check_ico
	# stamina 1 - 3
	for n in range(1, 4):
		var heal_but = Button.new()
		var f_str = "Blades %d (req. Fighter level %d)"
		var a_str = f_str % [n, (n * 2)]
		heal_but.text = a_str
		heal_but.connect("pressed",Callable(self,"_blades_pressed").bind(n))
		right_side.add_child(heal_but)
		var TR1 = TextureRect.new()
		if healing_skill >= n:
			TR1.texture = chk_tex
		else:
			TR1.texture = cir_tex
		right_side.add_child(TR1)

func _blades_pressed(arg: int):
	var req_met = false
	if arg == 1 and fighter_skill >= 2:
		req_met = true
	if arg == 2 and fighter_skill >= 4:
		req_met = true
	if arg == 3 and fighter_skill >= 6:
		req_met = true
	if blades_skill == (arg - 1) and skillpoints > 0 and req_met:
		blades_skill = arg
		skillpoints -= 1
		update_exp()
		_on_fig_blades_pressed()
		_play_lvl()
	else:
		_play_error()

