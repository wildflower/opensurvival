extends Control

# /dialog/dialog_manager.gd

@export var _dialog_text_path: NodePath
@export var _avatar_path: NodePath
@export var _current_dialog: Dialog

var _current_slide_index := 0

var npc_name = ""

@export var game_pause := true

@onready var dialog_ui = $"."
@onready var hud = $"../Hud"
@onready var a_quest = $"../Hud/ActiveQuest"

@onready var player = $"../Player"

@onready var name_label = $Panel/NameLabel
@onready var but1 = $Panel/Button1
@onready var but2 = $Panel/Button2
@onready var but3 = $Panel/Button3

@onready var trade_but = $Panel/DevButton

@onready var player_ui := $"../TraderUI"
@onready var trader_ui := $"../TraderUI/StorageContainer"

@onready var journal := $"../PlayerUI/TabContainer/Journal/Panel/HBoxContainer/ScrollContainer/GridContainer"

@onready var _dialog_text: Label = get_node(_dialog_text_path)
@onready var _avatar: TextureRect = get_node(_avatar_path)

func _ready():
#	_avatar.texture = _current_dialog.avatar_texture
	GameEvents.connect("dialog_initiated", _on_dialog_initiated)

func _on_dialog_initiated(cur_npc) -> void:
	#shopbutton.visible = true
	hud.visible = false
	npc_name = cur_npc
	name_label.text = npc_name
	_current_dialog = load("res://dialog/%s.tres" % cur_npc)
	_current_slide_index = _current_dialog.step
	
	if npc_name == "Coop":
		# Trade butten visibility (add it to the dialog_class?)
		trade_but.visible = false
	if npc_name == "TestCube":
		trade_but.visible = false
		var dialog_var = get_node("/root/DialogVars")
		_current_slide_index = dialog_var.testcube_step
	if npc_name == "TraderBob":
		# find trader Bob quest done
		journal.j_step[1] = 2
		journal.update_ui()
		trade_but.visible = true
		var dialog_var = get_node("/root/DialogVars")
		_current_slide_index = dialog_var.traderbob_step
	
	if  game_pause:
		get_tree().paused = true
		game_pause = false
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		dialog_ui.visible = true
	else:
		get_tree().paused = false
		game_pause = true
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		dialog_ui.visible = false
	
	_avatar.texture = _current_dialog.avatar_texture
	show_slide()

# trading
func _on_button_pressed():
	_on_button_3_pressed()
	dialog_ui.visible = false
	player_ui.visible = true
	trader_ui.visible = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

# this button is used for positive messages that lead to other questions
func _on_button_1_pressed():
	# do we need to give an item
	if _current_dialog.item_name[_current_slide_index] != "":
		var i_name = _current_dialog.item_name[_current_slide_index]
		var i_quantity = _current_dialog.item_quantity[_current_slide_index]
		player.inventory.remove_item(i_name, i_quantity)
	# Reward
	if _current_dialog.reward_name[_current_slide_index] != "":
		if npc_name == "TraderBob" and _current_slide_index == 3:
			# trader Bob quest done
			a_quest.text = ""
		var r_name = _current_dialog.reward_name[_current_slide_index]
		var r_quantity = _current_dialog.reward_quantity[_current_slide_index]
		player.inventory.add_item(r_name, r_quantity)
	_current_slide_index = _current_dialog.pos_react[_current_slide_index]
	show_slide()

# this button is used for negative messages that lead to other questions
func _on_button_2_pressed():
	_current_slide_index = _current_dialog.neg_react[_current_slide_index]
	show_slide()

# this button is used for messages that ends the dialog
func _on_button_3_pressed():
	# do we need to give an item
#	if _current_dialog.item_name[_current_slide_index] != "":
#		var i_name = _current_dialog.item_name[_current_slide_index]
#		var i_quantity = _current_dialog.item_quantity[_current_slide_index]
#		player.inventory.remove_item(i_name, i_quantity)
#	# Reward
#	if _current_dialog.reward_name[_current_slide_index] != "":
#		var r_name = _current_dialog.reward_name[_current_slide_index]
#		var r_quantity = _current_dialog.reward_quantity[_current_slide_index]
#		player.inventory.add_item(r_name, r_quantity)
	_current_dialog.step = _current_dialog.clr_react[_current_slide_index]
	# save step for each npc
	var dialog_var = get_node("/root/DialogVars")
	if npc_name == "TestCube":
		dialog_var.testcube_step = _current_dialog.step
	if npc_name == "TraderBob":
		dialog_var.traderbob_step = _current_dialog.step
	
	get_tree().paused = false
	game_pause = true
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	dialog_ui.visible = false
	hud.visible = true

func show_slide() -> void:
	_dialog_text.text = _current_dialog.dialog_slides[_current_slide_index]
	
	# positive dialog: giving items, accepting quests and other acknowledgments
	# do we need to have an item in our inventory?
#	but1.text = _current_dialog.player_dialog_pos[_current_slide_index]
	if _current_dialog.item_quantity[_current_slide_index] > 0:
		# Do we have it in inventory?
		for item in player.inventory.get_items():
			if item.name == _current_dialog.item_name[_current_slide_index]:
				but1.text = _current_dialog.player_dialog_pos[_current_slide_index]
				break
			else:
				but1.text = ""
	else:
		but1.text = _current_dialog.player_dialog_pos[_current_slide_index]
	# No text, No button
	if but1.text.length() > 0:
		but1.visible = true
	else:
		but1.visible = false
	
	# negative dialog like insults, refusals and sutch
	# do we need to have an item in our inventory?
	#but2.text = _current_dialog.player_dialog_neg[_current_slide_index]
	if _current_dialog.item_quantity[_current_slide_index] > 0:
		# Do we have it in inventory?
		for item in player.inventory.get_items():
			if item.name == _current_dialog.item_name[_current_slide_index]:
				but2.text = _current_dialog.player_dialog_neg[_current_slide_index]
				break
			else:
				but2.text = ""
	else:
		but2.text = _current_dialog.player_dialog_neg[_current_slide_index]
	# No text, No button
	if but2.text.length() > 0:
		but2.visible = true
	else:
		but2.visible = false
	
	# close/clear dialog
	but3.text = _current_dialog.player_dialog_clr[_current_slide_index]
	# No text, No button
	if but3.text.length() > 0:
		but3.visible = true
	else:
		but3.visible = false


func _on_trader_exit_pressed():
	player_ui.visible = false
	trader_ui.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
