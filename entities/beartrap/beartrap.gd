extends Node3D

# /entities/beartrap/beartrap.gd

@onready var player := get_node("/root/MainScene/Player")

@onready var ani_trap = $AnimationPlayer

var audio_player = preload("res://sound/audio_player.tscn")
var audio_trap = preload("res://sound/beartrap.wav")

var trap_closed = false

func _ready():
	pass # Replace with function body.

func _on_area_3d_body_entered(_body):
	if _body.is_in_group("zombie"):
		if !trap_closed:
			_body.health = 0
			trap_closed = true
			ani_trap.play("beartrap_anim", -1 , 1.0, false)
			#play_sound(audio_trap, 0.3, 0.4)
			var sound = audio_player.instantiate()
			self.add_child(sound)
			sound.play_sound(audio_trap)
	if _body.name == "Player":
		if !trap_closed:
			GameEvents.emit_signal("player_damage", 30)
			trap_closed = true
			ani_trap.play("beartrap_anim", -1 , 1.0, false)
			#play_sound(audio_trap, 0.3, 0.4)
			var sound = audio_player.instantiate()
			self.add_child(sound)
			sound.play_sound(audio_trap)
