extends Node3D

@export var chicken_amt: int
@export var egg_amt: int

func _ready():
	chicken_amt = 1
	egg_amt = 0

func _on_day_have_passed():
	# chickens lay one egg every day
	egg_amt = chicken_amt + egg_amt
