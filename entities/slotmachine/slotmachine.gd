extends Node3D

# slotmachine.gd

@onready var ani := $"AnimationPlayer"
@onready var s1 := $"slot_1"
@onready var s2 := $"slot_2"
@onready var s3 := $"slot_3"
@onready var zspawn := $Zspawn

@onready var l1 := $hold_1/light_1
@onready var l2 := $hold_2/light_2
@onready var l3 := $hold_3/light_3

@onready var carlo = preload("res://enemies/carlo/carlo.tscn")

var audio_player = preload("res://sound/audio_player.tscn")
var audio_win = preload("res://sound/winner.wav")
var audio_spin = preload("res://sound/spin.wav")

var running = false

var can_hold = false

var hold_1 = false
var hold_2 = false
var hold_3 = false

var pos = [0,45,90,135,180,225,270,315]

var slot = ["seven","peach","lemon","cherry","melon","bar","dollar","diamond"]
var win  = [100, 5, 10, 1, 20, 40, 30, 50]

var payout = 0

var rng = RandomNumberGenerator.new()

var randi_1: int
var randi_2: int
var randi_3: int

func _ready():
	rng.randomize()

func _process(_delta):
	if running:
		if !hold_1:
			s1.rotate_x(0.12)
		if !hold_2:
			s2.rotate_x(0.11)
		if !hold_3:
			s3.rotate_x(0.13)

func run_slot():
	can_hold = true
	if !running:
		var sound = audio_player.instantiate()
		self.add_child(sound)
		sound.play_sound(audio_spin)
		ani.play("slotarm_action", -1 , 1.0, false)
		running = true
		await get_tree().create_timer(2).timeout
		_stop_run()

func _stop_run():
	running = false
	if !hold_1:
		randi_1 = rng.randi_range(0, 7)
		s1.set_rotation_degrees(Vector3(pos[randi_1],0,0))
	if !hold_2:
		randi_2 = rng.randi_range(0, 7)
		s2.set_rotation_degrees(Vector3(pos[randi_2],0,0))
	if !hold_3:
		randi_3 = rng.randi_range(0, 7)
		s3.set_rotation_degrees(Vector3(pos[randi_3],0,0))
	
	if hold_1 == true:
		can_hold = false
		hold_1 = false
		var material = l1.mesh.surface_get_material(0)
		material.albedo_color = Color(0, 1, 0)
		l1.mesh.surface_set_material(0, material)
	if hold_2 == true:
		can_hold = false
		hold_2 = false
		var material2 = l2.mesh.surface_get_material(0)
		material2.albedo_color = Color(0, 1, 0)
		l2.mesh.surface_set_material(0, material2)
	if hold_3 == true:
		can_hold = false
		hold_3 = false
		var material3 = l3.mesh.surface_get_material(0)
		material3.albedo_color = Color(0, 1, 0)
		l3.mesh.surface_set_material(0, material3)
	
	# one and two cherry
	payout = 0
	if randi_1 == 3:
		payout = 1
		if randi_2 == 3:
			payout = 2
		_winning()
		print("You have won %d" % payout)
	
	# three alike
	if randi_1 == randi_2 and randi_1 == randi_3:
		payout = win[randi_1] + win[randi_2] + win[randi_3]
		_winning()
		print("You have won %d" % payout)

func _winning():
	can_hold = false
	var sound = audio_player.instantiate()
	self.add_child(sound)
	sound.play_sound(audio_win)
	spawn_zombie()

func spawn_zombie():
	# get Marker3D positions
	var zpos = zspawn.global_transform.origin
	# make zombie instance and add to scene
	var zombie = carlo.instantiate() as Node3D
	get_tree().get_root().add_child(zombie)
	zombie.global_transform.origin = zpos
	zombie.health = 100

func hold_slot(slot_nr):
	if can_hold and !running:
		if slot_nr == "hold_1":
			hold_1 = true
			var material = l1.mesh.surface_get_material(0)
			material.albedo_color = Color(1, 0, 0)
			l1.mesh.surface_set_material(0, material)
		if slot_nr == "hold_2":
			hold_2 = true
			var material = l2.mesh.surface_get_material(0)
			material.albedo_color = Color(1, 0, 0)
			l2.mesh.surface_set_material(0, material)
		if slot_nr == "hold_3":
			hold_3 = true
			var material = l3.mesh.surface_get_material(0)
			material.albedo_color = Color(1, 0, 0)
			l3.mesh.surface_set_material(0, material)
