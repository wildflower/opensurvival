extends Node3D

@export var water_amt: int
@export var tincan_amt: int

var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	var randi_1 = rng.randi_range(0, 2)
	water_amt = 0
	tincan_amt = randi_1

