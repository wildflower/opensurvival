extends Node3D

# stove.gd

@export var water_amt: int
@export var tincan_amt: int
@export var pan_amt: int

func _ready():
	water_amt = 2
	tincan_amt = 0
	pan_amt = 1

