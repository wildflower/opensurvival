extends Node3D

# watercollector.gd

@export var water_amt: int

func _ready():
	water_amt = 1

func _on_day_have_passed():
	# collect three water every day
	water_amt += 3
