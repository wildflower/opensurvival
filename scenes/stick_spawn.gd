extends Marker3D

# stick_spawn.gd

@onready var loot = preload("res://inventory/stick.tscn")

var pos

func _ready():
	# get Marker3D position for loot
	pos = self.global_transform.origin
	# wait for scene to load before planting loot
	await get_tree().create_timer(0.2).timeout
	plant_loot()

func plant_loot():
	# make loot instance and add to scene
	var b = loot.instantiate() as Node3D
	get_tree().get_root().add_child(b)
	b.global_transform.origin = pos
	# random value for y-rotation
	# random value for scale and y-rotation
	var s_val = randf_range ( 0.8, 2.2 )
	var msh = b.get_node("Cube")
	msh.scale = Vector3(s_val, s_val, s_val)
	msh.rotation.y = s_val
