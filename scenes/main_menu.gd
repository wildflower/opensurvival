extends Control

@onready var menu := $MainPanel
@onready var load_label := $LoadLabel
@onready var credits := $CreditsPanel
@onready var settings := $SettingsPanel

func _on_new_game_but_pressed():
	menu.visible = false
	load_label.visible = true
	await get_tree().create_timer(0.1).timeout
	get_tree().change_scene_to_file("res://scenes/main_scene.tscn")

func _on_load_game_but_pressed():
	pass

func _on_settings_but_pressed():
	menu.visible = false
	settings.visible = true

func _on_credits_but_pressed():
	menu.visible = false
	credits.visible = true

func _on_exit_but_pressed():
	get_tree().quit();

func _on_button_pressed():
	credits.visible = false
	menu.visible = true

func _on_b_button_pressed():
	settings.visible = false
	menu.visible = true
