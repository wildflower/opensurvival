extends Node3D

# trader_poi.gd

@onready var ani := $"AnimationPlayer"
@onready var ani_door := $"trader_door/AnimationPlayer"

@onready var journal_ui := $"../PlayerUI/TabContainer/Journal/Panel/HBoxContainer/ScrollContainer/GridContainer"

var audio_player = preload("res://sound/audio_player.tscn")
var audio_door = preload("res://sound/doorcreak.wav")

var doorlocked = true
var tut_done = false

func _ready():
	ani.play("shopkeeper", -1 , 1.0, false)

func _on_area_3d_body_entered(_body):
	if _body.name == "Player":
		if !tut_done:
			if journal_ui.j_step[0] > 1:
				tut_done = true
				if doorlocked:
					ani_door.play("traderdoor_ani", -1 , 1.0, false)
					var sound = audio_player.instantiate()
					self.add_child(sound)
					sound.play_sound(audio_door)
					doorlocked = false
		else:
			if doorlocked:
				ani_door.play("traderdoor_ani", -1 , 1.0, false)
				var sound = audio_player.instantiate()
				self.add_child(sound)
				sound.play_sound(audio_door)
				doorlocked = false

func _on_area_3d_body_exited(_body):
	if _body.name == "Player" and doorlocked == false:
		ani_door.play_backwards("traderdoor_ani", -1)
		var sound = audio_player.instantiate()
		self.add_child(sound)
		sound.play_sound(audio_door)
		doorlocked = true

