extends Marker3D

# loot_bag_spawn.gd

@onready var loot = preload("res://entities/player_storage/player_storage.tscn")

var pos

func _ready():
	# get Marker3D position for loot
	pos = self.global_transform.origin
	# wait for scene to load before planting loot
	await get_tree().create_timer(0.2).timeout
	plant_loot()

func plant_loot():
	# make loot instance and add to scene
	var b = loot.instantiate() as Node3D
	get_tree().get_root().add_child(b)
	b.global_transform.origin = pos
