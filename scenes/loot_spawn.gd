extends Marker3D

# loot_spawn.gd

@onready var loot = preload("res://inventory/lockpick.tscn")

var pos

func _ready():
	# get Marker3D position for loot
	pos = self.global_transform.origin
	# wait for scene to load before planting loot
	await get_tree().create_timer(0.2).timeout
	plant_loot()

func _input(_event):
	if Input.is_key_pressed(KEY_L):
		plant_loot()

func plant_loot():
	# make loot instance and add to scene
	var b = loot.instantiate() as Node3D
	get_tree().get_root().add_child(b)
	b.global_transform.origin = pos
	# random value for scale and y-rotation
	var s_val = randf_range ( 0.8, 2.2 )
	var msh = b.get_node("lockpick2")
	msh.rotation.y = s_val
