extends Node3D

# diner.gd

@onready var sink = preload("res://entities/kitchen/kitchensink.tscn")
@onready var cab = preload("res://entities/kitchen/kitchencab.tscn")
@onready var draw = preload("res://entities/kitchen/kitchendraw.tscn")
@onready var fridge = preload("res://entities/kitchen/fridge.tscn")
@onready var stove = preload("res://entities/kitchen/stove.tscn")
@onready var trash = preload("res://entities/kitchen/trashcan.tscn")
@onready var sink_pos = $KitchenSink
@onready var cab_pos = $KitchenCab
@onready var cab2_pos = $KitchenCab2
@onready var draw_pos = $KitchenDraw
@onready var fridge1_pos = $Fridge1
@onready var fridge2_pos = $Fridge2
@onready var stove_pos = $Stove
@onready var trash_pos = $Trashcan

var a_pos
var b_pos
var c_pos
var d_pos
var e_pos
var f_pos
var g_pos
var h_pos

func _ready():
	# get Marker3D position for loot
	a_pos = sink_pos.global_transform.origin
	b_pos = cab_pos.global_transform.origin
	c_pos = draw_pos.global_transform.origin
	d_pos = cab2_pos.global_transform.origin
	e_pos = fridge1_pos.global_transform.origin
	f_pos = fridge2_pos.global_transform.origin
	g_pos = stove_pos.global_transform.origin
	h_pos = trash_pos.global_transform.origin
	# wait for scene to load before planting loot
	await get_tree().create_timer(0.2).timeout
	place_models()

func place_models():
	# make loot instance and add to scene
	var a = sink.instantiate() as Node3D
	get_tree().get_root().add_child(a)
	a.global_transform.origin = a_pos
	
	var b = cab.instantiate() as Node3D
	get_tree().get_root().add_child(b)
	b.global_transform.origin = b_pos
	
	var c = draw.instantiate() as Node3D
	get_tree().get_root().add_child(c)
	c.global_transform.origin = c_pos
	
	var d = cab.instantiate() as Node3D
	get_tree().get_root().add_child(d)
	d.global_transform.origin = d_pos
	
	var e = fridge.instantiate() as Node3D
	get_tree().get_root().add_child(e)
	e.global_transform.origin = e_pos
	
	var f = fridge.instantiate() as Node3D
	get_tree().get_root().add_child(f)
	f.global_transform.origin = f_pos
	
	var g = stove.instantiate() as Node3D
	get_tree().get_root().add_child(g)
	g.global_transform.origin = g_pos
	
	var h = trash.instantiate() as Node3D
	get_tree().get_root().add_child(h)
	h.global_transform.origin = h_pos

