extends Node3D

# /scenes/poi/gasstation.gd

@onready var carlo = preload("res://enemies/carlo/carlo.tscn")
@onready var loot = preload("res://inventory/tincan.tscn")
@onready var chest = preload("res://entities/loot_chest/loot_chest.tscn")

@onready var z1spawn := $Z1Spawn
@onready var z2spawn := $Z2Spawn

@onready var l1spawn := $L1Spawn
@onready var l2spawn := $L2Spawn

@onready var journal := get_node("/root/MainScene/PlayerUI/TabContainer/Journal/Panel/HBoxContainer/ScrollContainer/GridContainer")

@onready var skilltree := get_node("/root/MainScene/PlayerUI/TabContainer/Skilltree/Panel/HBoxContainer")

var z_count = 2

func _ready():
	# wait for scene to load
	await get_tree().create_timer(0.2).timeout
	place_zombies()
	place_loot()

func place_zombies():
	# get Marker3D positions
	var pos1 = z1spawn.global_transform.origin
	var pos2 = z2spawn.global_transform.origin
#	for n in z_count:
#		print("spawn zombie")
	# make zombie instance and add to scene
	var zombie = carlo.instantiate() as Node3D
	get_tree().get_root().add_child(zombie)
	zombie.global_transform.origin = pos1
	zombie.connect("tree_exiting", _on_tree_exiting)
	zombie.health = 100
	# make zombie instance and add to scene
	var zombie2 = carlo.instantiate() as Node3D
	get_tree().get_root().add_child(zombie2)
	zombie2.global_transform.origin = pos2
	zombie2.connect("tree_exiting", _on_tree_exiting)
	zombie2.health = 100

func place_loot():
	# get Marker3D positions
	var pos1 = l1spawn.global_transform.origin
	var pos2 = l2spawn.global_transform.origin
	# make loot instance and add to scene
	var food = loot.instantiate() as Node3D
	get_tree().get_root().add_child(food)
	food.global_transform.origin = pos1
	# locked loot chest
	var chest1 = chest.instantiate() as Node3D
	get_tree().get_root().add_child(chest1)
	chest1.global_transform.origin = pos2

func _on_tree_exiting():
	if journal.cur_quest == "Clear gasstation":
		z_count -= 1
		if z_count == 0:
			# set quest to [done]
			journal.j_step[(journal.j_step.size()-1)] = 2
			journal.cur_quest = "Quest complete, return for your reward"
			journal.update_ui()
			# experience
			skilltree.experience += 200
			skilltree.update_exp()
			# set trader-bob reward dialog
			var dialog_var = get_node("/root/DialogVars")
			dialog_var.traderbob_step = 3

