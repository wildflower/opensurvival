extends Resource

# recipe.gd

class_name Recipe

# recipe requirement 1, 2 and 3 (leave empty if not used)
@export var r_req_1: String
@export var r_req_2: String
@export var r_req_3: String

# text description
@export var r_text: String

# image - chouldnt we get it from inventory item?
@export var r_img: Texture
