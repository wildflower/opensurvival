extends GridContainer

# /inventory/crafting_ui.gd

@onready var preview_label := $"../../ScrollContainer2/VBoxContainer/Label"
@onready var message := $"../../../../../../../Hud/Messages"
@onready var player := $"../../../../../../../Player"

@onready var journal_ui := $"../../../../../Journal/Panel/HBoxContainer/ScrollContainer/GridContainer"

var audio_player = preload("res://sound/audio_player.tscn")
var audio_error = preload("res://sound/error.wav")
var audio_craft = preload("res://sound/crafting.wav")

var recipe = load("res://inventory/recipe/fiber.tres")

var tut_done = false

#TODO: loop through recipes to fill this array
var all_rec = [
	"club",
	"fiber",
	"firewood",
	"frame",
	"stoneaxe",
]

func _ready():
	_update_ui()

func _update_ui():
	# remove_at existing items from grid
	for n in get_children():
		n.queue_free()
	
	# labels for grid
	var name_label = Label.new()
	name_label.text = "    Recipe name     "
	add_child(name_label)
	var img_label = Label.new()
	img_label.text = " - "
	add_child(img_label)

	# recipe button and image
	for r_item in all_rec.size():
		var item_button = Button.new()
		item_button.text = "   %s   " % all_rec[r_item]
		item_button.connect("pressed",Callable(self,"_button_pressed").bind(all_rec[r_item]))
		item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(all_rec[r_item]))
		item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
		add_child(item_button)
		recipe = load("res://inventory/recipe/%s.tres" % all_rec[r_item])
		var texture = ImageTexture.new()
		texture = recipe.r_img
		var TR = TextureRect.new()
		TR.texture = texture
		add_child(TR)

func _on_btn_mouse_entered(extra_arg_0: String):
	recipe = load("res://inventory/recipe/%s.tres" % extra_arg_0)
	# show item info on the right side
	preview_label.text = recipe.r_text
	preview_label.visible = true
func _on_btn_mouse_exited():
	preview_label.visible = false

func _button_pressed(extra_arg_0: String):
	# load data from resource file
	recipe = load("res://inventory/recipe/%s.tres" % extra_arg_0)
	# check for required materials
	var req_met = false
	if player.inventory.get_item(recipe.r_req_1):
		req_met = true
		if recipe.r_req_2:
			if player.inventory.get_item(recipe.r_req_2):
				req_met = true
			else:
				req_met = false
			if recipe.r_req_3:
				if player.inventory.get_item(recipe.r_req_3):
					req_met = true
				else:
					req_met = false
	if req_met:
		# remove materials from inventory
		player.inventory.remove_item(recipe.r_req_1, 1)
		if recipe.r_req_2:
			player.inventory.remove_item(recipe.r_req_2, 1)
		if recipe.r_req_3:
			player.inventory.remove_item(recipe.r_req_3, 1)
		# add crafted item to inventory
		player.inventory.add_item(extra_arg_0, 1)
		message.text = "You crafted %s" % extra_arg_0
		#play_sound(audio_craft, 0.5, 0.5)
		var sound = audio_player.instantiate()
		self.add_child(sound)
		sound.play_sound(audio_craft)
		if !tut_done:
			if extra_arg_0 == "stoneaxe":
				journal_ui.cur_quest = "FindTraderBob"
				# add the next journal-entry
				journal_ui.j_entry.append("Find trader Bob")
				journal_ui.j_text.append("Find trader Bobs compound")
				journal_ui.j_step.append(1)
				# set status for previous journal-entry
				journal_ui.j_step[(journal_ui.j_step.size()-2)] = 2
				journal_ui.update_ui()
				tut_done = true
	else:
		message.text = "You do not have the required materials"
		#play_sound(audio_error, 0.5, 0.5)
		var sound = audio_player.instantiate()
		self.add_child(sound)
		sound.play_sound(audio_error)
