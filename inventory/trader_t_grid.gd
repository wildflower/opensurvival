extends GridContainer

# storage_container.gd

@onready var message := get_node("/root/MainScene/Hud/Messages")

@onready var preview_label := $"../../../../TabContainer/Inventory/Panel/HBoxContainer/ScrollContainer2/VBoxContainer/Label"
@onready var player := $"../../../../../Player"

var audio_player = preload("res://sound/audio_player.tscn")
var audio_error = preload("res://sound/error.wav")
var audio_coins = preload("res://sound/coins.wav")

var bob_inventory = load("res://inventory/bob_inventory.tres")

func _ready():
	_testinv()

func _testinv():
	# remove_at existing items from grid
	for n in get_children():
		n.queue_free()
	
	var a_label = Label.new()
	a_label.text = " Trader Bob "
	add_child(a_label)
	var b_label = Label.new()
	b_label.text = " "
	add_child(b_label)
	var c_label = Label.new()
	c_label.text = " "
	add_child(c_label)
	var d_label = Label.new()
	d_label.text = " "
	add_child(d_label)
	# should add a category to sort by
	var e_label = Label.new()
	e_label.text = " "
	add_child(e_label)

	# labels for grid
	var name_label = Label.new()
	name_label.text = "    ItemName     "
	add_child(name_label)
	var amount_label = Label.new()
	amount_label.text = " - - "
	add_child(amount_label)
	var weight_label = Label.new()
	weight_label.text = " - - "
	add_child(weight_label)
	var value_label = Label.new()
	value_label.text = " val "
	add_child(value_label)
	# should add a category to sort by
	var img_label = Label.new()
	img_label.text = " "
	add_child(img_label)

	# Inventory items
	for item in bob_inventory.get_items():
		# we only want one type of items
#		if item.item_reference.type == selected_type:
		var item_button = Button.new()
		item_button.text = "   %s   " % [item.name]
		item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
		item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
		item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
#		item_button.minimum_size.x = 180
		add_child(item_button)
		
		var q_label = Label.new()
		q_label.text = "  -  "
		add_child(q_label)
	
		var w_label = Label.new()
		w_label.text = "  -  "
		add_child(w_label)
		
		var v_label = Label.new()
		v_label.text = "  %d  " % [item.value]
		add_child(v_label)
		
		var texture = ImageTexture.new()
		texture = item.texture
		var TR = TextureRect.new()
		TR.texture = texture
		add_child(TR)

func _button_pressed(extra_arg_0: String):
	var player_coin = 0
	if player.inventory.get_item("coin"):
		player_coin = player.inventory.get_item_amt("coin")
		# buy from trader
		if extra_arg_0 == "firstaid":
			# do player have enough coin
			if player_coin >= 50:
				# use coin
				player.inventory.remove_item("coin", 50)
				#play_sound(audio_eat, 0.5, 0.5)
				player.inventory.add_item("firstaid", 1)
				message.text = "You bought a firstaid kit"
				_play_coins()
			else:
				message.text = "You dont have enough coin"
				_play_error()
		if extra_arg_0 == "tincan":
			# do player have enough coin
			if player_coin >= 15:
				# use coin
				player.inventory.remove_item("coin", 15)
				#play_sound(audio_drink, 0.5, 0.5)
				player.inventory.add_item("tincan", 1)
				message.text = "You bought a tincan"
				_play_coins()
			else:
				message.text = "You dont have enough coin"
				_play_error()
		if extra_arg_0 == "lockpick":
			# do player have enough coin
			if player_coin >= 99:
				# use coin
				player.inventory.remove_item("coin", 99)
				#play_sound(audio_drink, 0.5, 0.5)
				player.inventory.add_item("lockpick", 1)
				message.text = "You bought a lockpick"
				_play_coins()
			else:
				message.text = "You dont have enough coin"
				_play_error()
	else:
		message.text = "You dont have any coin"
		_play_error()

func _on_btn_mouse_entered(extra_arg_0: String):
	# show item info on the right side
	preview_label.text = extra_arg_0
	preview_label.visible = true

func _on_btn_mouse_exited():
	# hide item info on the right side
	preview_label.visible = false

func _play_error():
	var sound = audio_player.instantiate()
	player.add_child(sound)
	sound.play_sound(audio_error)
func _play_coins():
	var sound = audio_player.instantiate()
	player.add_child(sound)
	sound.play_sound(audio_coins)
