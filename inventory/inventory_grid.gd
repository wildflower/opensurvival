extends GridContainer

@onready var preview_label := $"../../ScrollContainer2/VBoxContainer/Label"
@onready var toolbelt_menu := $"../../../ToolbeltMenu"

@onready var tool_1 := $"../../../../../../../Hud/Toolbelt/Tool1"
@onready var tool_2 := $"../../../../../../../Hud/Toolbelt/Tool2"
@onready var tool_3 := $"../../../../../../../Hud/Toolbelt/Tool3"
@onready var tool_4 := $"../../../../../../../Hud/Toolbelt/Tool4"
@onready var tool_5 := $"../../../../../../../Hud/Toolbelt/Tool5"
@onready var tool_6 := $"../../../../../../../Hud/Toolbelt/Tool6"
@onready var tool_7 := $"../../../../../../../Hud/Toolbelt/Tool7"
@onready var tool_8 := $"../../../../../../../Hud/Toolbelt/Tool8"
@onready var tool_9 := $"../../../../../../../Hud/Toolbelt/Tool9"
@onready var tool_0 := $"../../../../../../../Hud/Toolbelt/Tool0"

@onready var player := $"../../../../../../../Player"

@onready var note_ui := $"../../../../../../../TutorialUI"

# get active tool var from playerhands/shootraycast in shoot.gd
@onready var shoot_ray := get_node("/root/MainScene/Player/Neck/Camera3D/playerhands/ShootRayCast")

@onready var message := get_node("/root/MainScene/Hud/Messages")

var audio_player = preload("res://sound/audio_player.tscn")
var audio_drink = preload("res://sound/drinking.wav")
var audio_eat = preload("res://sound/eat_item.wav")
var audio_pills = preload("res://sound/pills.wav")

signal hud_update()

var clicked_tool = "fist"
var active_tool = "fist"

func _ready():
	randomize()
	active_tool = shoot_ray.toolbelt
	player.inventory.connect("items_changed", _testinv)
	_testinv()

func _testinv():
	# remove_at existing items from grid
	for n in get_children():
		n.queue_free()
	
	# labels for grid
	var name_label = Label.new()
	name_label.text = "    ItemName     "
	add_child(name_label)
	
	var amount_label = Label.new()
	amount_label.text = "- amt"
	add_child(amount_label)
	
	var weight_label = Label.new()
	weight_label.text = "- wgt"
	add_child(weight_label)
	
	var value_label = Label.new()
	value_label.text = "- val"
	add_child(value_label)
	
	# should add a category to sort by
	
	var img_label = Label.new()
	img_label.text = " "
	add_child(img_label)

	# Inventory items
	for item in player.inventory.get_items():
		# we only want one type of items
#		if item.item_reference.type == selected_type:
		var item_button = Button.new()
		item_button.text = "   %s   " % [item.name]
		item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
		item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
		item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
		add_child(item_button)
		
		var q_label = Label.new()
		q_label.text = "  %d  " % [item.amount]
		add_child(q_label)
	
		var w_label = Label.new()
		w_label.text = "  %d  " % [item.weight]
		add_child(w_label)
		
		var v_label = Label.new()
		v_label.text = "  %d  " % [item.value]
		add_child(v_label)
		
		# should add a category to sort by
		
		var texture = ImageTexture.new()
		texture = item.texture
		var TR = TextureRect.new()
		TR.texture = texture
		add_child(TR)

func _button_pressed(arg: String):
	var item = player.inventory.get_item(arg)
	if item.type == 0: # GENERIC
		if arg == "note":
			note_ui.visible = true
	if item.type == 1: # CONSUMABLE
		if item.healing > 0:
			var sound = audio_player.instantiate()
			player.add_child(sound)
			sound.play_sound(audio_pills)
			player.health += item.healing
			if player.health > 100:
				player.health = 100
		if item.healing < 0:
			player.health += item.healing
			if player.health < 1:
				print("player death")
		if item.stamina > 0:
			player.stamina += item.stamina
			if player.stamina > 100:
				player.stamina = 100
		if item.food > 0:
			if arg == "tincan":
				# foodpoison (remember to add empty can after)
				if (randi() % 10) > 5:
					player.health -= 20
			player.hunger += item.food
			if player.hunger > 100:
				player.hunger = 100
			var sound = audio_player.instantiate()
			player.add_child(sound)
			sound.play_sound(audio_eat)
		if item.food < 0:
			player.hunger += item.food
		if item.drink > 0:
			if arg == "water_clean" or arg == "water_dirty":
				player.inventory.add_item("water_empty", 1)
			player.thirst += item.drink
			if player.thirst > 100:
				player.thirst = 100
			var sound = audio_player.instantiate()
			player.add_child(sound)
			sound.play_sound(audio_drink)
		if item.drink < 0:
			player.thirst += item.drink
		player.inventory.remove_item(arg, 1)
		emit_signal("hud_update")
	if item.type == 2: # QUEST
		print("QUEST")
	if item.type == 3: # EQUIPMENT
		clicked_tool = arg
		toolbelt_menu.visible = true

func _on_btn_mouse_entered(arg: String):
	# show item info on the right side
	preview_label.text = arg
	preview_label.visible = true

func _on_btn_mouse_exited():
	# hide item info on the right side
	preview_label.visible = false

#func update_all_items():
#	for item_index in player.inventory.items.size():
#		var item = player.inventory.items[item_index]

#func display_item(item):
#	if item is Item:
#		tex1.texture = item.texture
#		#itemAmountLabel.text = str(item.amount)
#	else:
#		tex1.texture = load("res://items/empty.png")
#		#itemAmountLabel.text = ""

func update_inventory_display_org():
	for item_index in player.inventory.items.size():
		update_item(item_index)

func update_item(item_index):
	var inventorySlotDisplay = get_child(item_index)
	var item = player.inventory.items[item_index]
	inventorySlotDisplay.display_item(item)
#	if item is Item:
#		tex1.texture = item.texture
		#itemAmountLabel.text = str(item.amount)
#	else:
#		tex1.texture = load("res://items/empty.png")
		#itemAmountLabel.text = ""

func _on_items_changed(indexes):
	for item_index in indexes:
		update_item(item_index)

# Toolbelt menu
func _on_toolbelt_cancel_pressed():
	toolbelt_menu.visible = false
func _on_button_1_pressed():
	message.text = "Frames are locked into key 1 for now"
#	shoot_ray.toolbelt = clicked_tool
#	shoot_ray.tool_1 = clicked_tool
#	tool_1.texture = load("res://ui/" + clicked_tool + ".png")
#	toolbelt_menu.visible = false
func _on_button_2_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_2 = clicked_tool
	tool_2.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false
func _on_button_3_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_3 = clicked_tool
	tool_3.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false
func _on_button_4_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_4 = clicked_tool
	tool_4.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false
func _on_button_5_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_5 = clicked_tool
	tool_5.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false
func _on_button_6_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_6 = clicked_tool
	tool_6.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false
func _on_button_7_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_7 = clicked_tool
	tool_7.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false
func _on_button_8_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_8 = clicked_tool
	tool_8.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false
func _on_button_9_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_9 = clicked_tool
	tool_9.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false
func _on_button_0_pressed():
	shoot_ray.toolbelt = clicked_tool
	shoot_ray.tool_0 = clicked_tool
	tool_0.texture = load("res://ui/" + clicked_tool + ".png")
	toolbelt_menu.visible = false

# clearing toolbelt slots
func _on_button_clear_1_pressed():
	message.text = "Frames are locked into key 1 for now"
#	shoot_ray.tool_1 = "fist"
#	tool_1.texture = load("res://ui/empty.png")
func _on_button_clear_2_pressed():
	shoot_ray.tool_2 = "fist"
	tool_2.texture = load("res://ui/empty.png")
func _on_button_clear_3_pressed():
	shoot_ray.tool_3 = "fist"
	tool_3.texture = load("res://ui/empty.png")
func _on_button_clear_4_pressed():
	shoot_ray.tool_4 = "fist"
	tool_4.texture = load("res://ui/empty.png")
func _on_button_clear_5_pressed():
	shoot_ray.tool_5 = "fist"
	tool_5.texture = load("res://ui/empty.png")
func _on_button_clear_6_pressed():
	shoot_ray.tool_6 = "fist"
	tool_6.texture = load("res://ui/empty.png")
func _on_button_clear_7_pressed():
	shoot_ray.tool_7 = "fist"
	tool_7.texture = load("res://ui/empty.png")
func _on_button_clear_8_pressed():
	shoot_ray.tool_8 = "fist"
	tool_8.texture = load("res://ui/empty.png")
func _on_button_clear_9_pressed():
	shoot_ray.tool_9 = "fist"
	tool_9.texture = load("res://ui/empty.png")
func _on_button_clear_0_pressed():
	shoot_ray.tool_0 = "fist"
	tool_0.texture = load("res://ui/empty.png")
