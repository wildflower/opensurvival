extends Resource

class_name Inventory

signal items_changed(indexes)

@export var items: Array[Resource] = []

func get_items():
	return items

func get_item(item_name):
	for i in items:
		if i.name == item_name:
			return i
	return null

func get_item_amt(item_name):
	for i in items:
		if i.name == item_name:
			var amt = i.amount
			return amt
	return null

func add_item(item_name, amount):
	if amount <= 0:
		print("Cant add a negative number of items")
		return
	
	#var item = ItemDatabase.get_item(item_name)
	var item = get_item(item_name)
	if not item:
#		print("Unknown item not added to inventory")
		# hack to add non-existing items
		# this should be fixed asap
		items.append(load("res://inventory/items/item_res/%s.tres" % item_name))
		# running the func from inside the func, what could go wrong :-)
		# just DONT pick up something not in item_res
		# for some reason frames duplicate when picked up???
		if item_name == "frame":
			emit_signal("items_changed")
			return
		else:
			add_item(item_name, amount)
			return
		#emit_signal("items_changed")
		#return
	
	var remaining_amount = amount
	var max_stack_size = item.max_stack_size if item.stackable else 1
	
	if item.stackable:
		for i in range(items.size()):
			if remaining_amount == 0:
				break
			
			var inventory_item = items[i]
			if inventory_item.name != item.name:
				continue
			
			if inventory_item.amount < inventory_item.max_stack_size:
				var original_amount = inventory_item.amount
				inventory_item.amount = min(original_amount + remaining_amount, max_stack_size)
				remaining_amount -= inventory_item.amount - original_amount
	while remaining_amount > 0:
		var new_item = {
			item_reference = item,
			quantity = min(remaining_amount, max_stack_size)
		}
		items.append(new_item)
		remaining_amount -= new_item.amount
	emit_signal("items_changed")

func remove_item(item_name, amount):
	if amount <= 0:
		print("Cant remove a negative number of items")
		return
	
	var item = get_item(item_name)
	if not item:
		print("Unknown item not added to inventory")
		return
	
	var remaining_amount = amount
	
	if item.stackable:
		var item_removed = false
		for i in range(items.size()):
			if remaining_amount == 0:
				break
			
			var inventory_item = items[i]
			if inventory_item.name != item.name:
				if item_removed == false:
					continue
				else:
					pass
			
			if inventory_item.amount > amount:
				inventory_item.amount -= amount
				emit_signal("items_changed")
				break
			else:
				inventory_item.amount -= 1
				emit_signal("items_changed")
				
				item_removed = true
				items.remove_at(i)
				emit_signal("items_changed")
				break

func set_item(item_index, item):
	var previousItem = items[item_index]
	items[item_index] = item
	emit_signal("items_changed", [item_index])
	return previousItem

func swap_items(item_index, target_item_index):
	var targetItem = items[target_item_index]
	var item = items[item_index]
	items[target_item_index] = item
	items[item_index] = targetItem
	emit_signal("items_changed", [item_index, target_item_index])

