extends GridContainer

@onready var preview_label := $"../../ScrollContainer2/VBoxContainer/Label"

@onready var player := $"../../../../../../../Player"

@onready var message := get_node("/root/MainScene/Hud/Messages")

var audio_player = preload("res://sound/audio_player.tscn")
var audio_coins = preload("res://sound/coins.wav")

signal hud_update()

func _ready():
	player.inventory.connect("items_changed", _testinv)
	_testinv()

func _testinv():
	# remove_at existing items from grid
	for n in get_children():
		n.queue_free()
	
	# labels for grid
	var name_label = Label.new()
	name_label.text = "    ItemName     "
	add_child(name_label)
	
	var amount_label = Label.new()
	amount_label.text = "- amt"
	add_child(amount_label)
	
	var weight_label = Label.new()
	weight_label.text = "- wgt"
	add_child(weight_label)
	
	var value_label = Label.new()
	value_label.text = "- val"
	add_child(value_label)
	
	# should add a category to sort by
	
	var img_label = Label.new()
	img_label.text = " "
	add_child(img_label)

	# Inventory items
	for item in player.inventory.get_items():
		# we only want one type of items
#		if item.item_reference.type == selected_type:
		var item_button = Button.new()
		item_button.text = "   %s   " % [item.name]
		item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
		item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
		item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
#		item_button.minimum_size.x = 180
		add_child(item_button)
		
		var q_label = Label.new()
		q_label.text = "  %d  " % [item.amount]
		add_child(q_label)
	
		var w_label = Label.new()
		w_label.text = "  %d  " % [item.weight]
		add_child(w_label)
		
		var v_label = Label.new()
		v_label.text = "  %d  " % [item.value]
		add_child(v_label)
		
		# should add a category to sort by
		
		var texture = ImageTexture.new()
		texture = item.texture
		var TR = TextureRect.new()
		TR.texture = texture
		add_child(TR)

func _button_pressed(extra_arg_0: String):
	# sell items
	if extra_arg_0 == "firstaid":
		player.inventory.remove_item("firstaid", 1)
		player.inventory.add_item("coin", 50)
		_play_coins()
		emit_signal("hud_update")
	if extra_arg_0 == "tincan":
		player.inventory.remove_item("tincan", 1)
		player.inventory.add_item("coin", 15)
		_play_coins()
		emit_signal("hud_update")
	if extra_arg_0 == "lockpick":
		player.inventory.remove_item("lockpick", 1)
		player.inventory.add_item("coin", 99)
		_play_coins()
		emit_signal("hud_update")

func _on_btn_mouse_entered(extra_arg_0: String):
	# show item info on the right side
	preview_label.text = extra_arg_0
	preview_label.visible = true

func _on_btn_mouse_exited():
	# hide item info on the right side
	preview_label.visible = false

#func update_all_items():
#	for item_index in player.inventory.items.size():
#		var item = player.inventory.items[item_index]

#func display_item(item):
#	if item is Item:
#		tex1.texture = item.texture
#		#itemAmountLabel.text = str(item.amount)
#	else:
#		tex1.texture = load("res://items/empty.png")
#		#itemAmountLabel.text = ""

func update_inventory_display_org():
	for item_index in player.inventory.items.size():
		update_item(item_index)

func update_item(item_index):
	var inventorySlotDisplay = get_child(item_index)
	var item = player.inventory.items[item_index]
	inventorySlotDisplay.display_item(item)
#	if item is Item:
#		tex1.texture = item.texture
		#itemAmountLabel.text = str(item.amount)
#	else:
#		tex1.texture = load("res://items/empty.png")
		#itemAmountLabel.text = ""

func _on_items_changed(indexes):
	for item_index in indexes:
		update_item(item_index)

func _play_coins():
	var sound = audio_player.instantiate()
	player.add_child(sound)
	sound.play_sound(audio_coins)
