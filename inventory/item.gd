extends Resource

class_name Item

@export var name: String = ""
@export var texture: Texture

@export var amount: int = 0

@export var stackable: bool = true
@export var max_stack_size: int = 99
@export var weight: int
@export var value: int

enum ItemType { GENERIC, CONSUMABLE, QUEST, EQUIPMENT }
@export var type: ItemType

# positive values for healing, negative for damage
@export var healing: int
@export var stamina: int
@export var food: int
@export var drink: int

@export var description: String = ""
