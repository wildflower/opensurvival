extends GridContainer

# storage_container.gd
# the left side panel of the LootingUI (container inventory)

@onready var message := get_node("/root/MainScene/Hud/Messages")

@onready var preview_label := $"../../../../TabContainer/Inventory/Panel/HBoxContainer/ScrollContainer2/VBoxContainer/Label"
@onready var player := $"../../../../../Player"

@onready var looting_ui := get_node("/root/MainScene/LootingUI")

@export var loot_dict = {}

var container_type = "player_storage"

var loot_inventory = load("res://inventory/loot_inventory.tres")

var container_obj: Node3D

func _ready():
	for item in loot_inventory.get_items():
		loot_dict[item.name] = 0
	loot_dict["amanita"] = 1
	loot_dict["apple"] = 1
	loot_dict["banana"] = 1
	loot_dict["bread"] = 1
	loot_dict["club"] = 1
	loot_dict["coin"] = 10
#	loot_dict["egg"] = 1
	loot_dict["firstaid"] = 1
	loot_dict["lockpick"] = 1
	loot_dict["tincan"] = 12
	loot_dict["fiber"] = 1
	loot_dict["water_clean"] = 1
	loot_dict["water_dirty"] = 1
	loot_dict["water_empty"] = 1
	_lootinv()

func _lootinv():
	# remove_at existing items from grid
	for n in get_children():
		n.queue_free()
	
	# top labels
	var a_label = Label.new()
	a_label.text = " Container "
	add_child(a_label)
	var b_label = Label.new()
	b_label.text = " "
	add_child(b_label)
	var c_label = Label.new()
	c_label.text = " "
	add_child(c_label)
	var d_label = Label.new()
	d_label.text = " "
	add_child(d_label)
	# should add a category to sort by
	var e_label = Label.new()
	e_label.text = " "
	add_child(e_label)

	# labels for grid
	var name_label = Label.new()
	name_label.text = "    ItemName     "
	add_child(name_label)
	var amount_label = Label.new()
	amount_label.text = " amt "
	add_child(amount_label)
	var weight_label = Label.new()
	weight_label.text = " wgt "
	add_child(weight_label)
	var value_label = Label.new()
	value_label.text = " val "
	add_child(value_label)
	# should add a category to sort by
	var img_label = Label.new()
	img_label.text = " "
	add_child(img_label)

	# loot items
	if container_type == "player_storage":
		for item in loot_inventory.get_items():
			if loot_dict[item.name] > 0:
				var item_button = Button.new()
				item_button.text = "   %s   " % [item.name]
				item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
				item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
				item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
				add_child(item_button)
				var q_label = Label.new()
				q_label.text = "  %d  " % (loot_dict[item.name])
				add_child(q_label)
				var w_label = Label.new()
				w_label.text = "  %d  " % [item.weight]
				add_child(w_label)
				var v_label = Label.new()
				v_label.text = "  %d  " % [item.value]
				add_child(v_label)
				var texture = ImageTexture.new()
				texture = item.texture
				var TR = TextureRect.new()
				TR.texture = texture
				add_child(TR)
			else:
				pass
	if container_type == "loot_bag":
		var obj_par = container_obj.get_parent()
		for item in loot_inventory.get_items():
			if item.name == "coin" and obj_par.coin_amt > 0:
				var item_button = Button.new()
				item_button.text = "   %s   " % [item.name]
				item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
				item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
				item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
				add_child(item_button)
				var q_label = Label.new()
				q_label.text = "  %d  " % (obj_par.coin_amt)
				add_child(q_label)
				var w_label = Label.new()
				w_label.text = "  %d  " % [item.weight]
				add_child(w_label)
				var v_label = Label.new()
				v_label.text = "  %d  " % [item.value]
				add_child(v_label)
				var texture = ImageTexture.new()
				texture = item.texture
				var TR = TextureRect.new()
				TR.texture = texture
				add_child(TR)
			else:
				pass
	if container_type == "coop":
		var obj_par = container_obj.get_parent()
		for item in loot_inventory.get_items():
			if item.name == "chicken" or item.name == "egg":
				if item.name == "chicken" and obj_par.chicken_amt > 0:
					var item_button = Button.new()
					item_button.text = "   %s   " % [item.name]
					item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
					item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
					item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
					add_child(item_button)
					var q_label = Label.new()
					q_label.text = "  %d  " % (obj_par.chicken_amt)
					add_child(q_label)
					var w_label = Label.new()
					w_label.text = "  %d  " % [item.weight]
					add_child(w_label)
					var v_label = Label.new()
					v_label.text = "  %d  " % [item.value]
					add_child(v_label)
					var texture = ImageTexture.new()
					texture = item.texture
					var TR = TextureRect.new()
					TR.texture = texture
					add_child(TR)
				if item.name == "egg" and obj_par.egg_amt > 0:
					var item_button = Button.new()
					item_button.text = "   %s   " % [item.name]
					item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
					item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
					item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
					add_child(item_button)
					var q_label = Label.new()
					q_label.text = "  %d  " % (obj_par.chicken_amt)
					add_child(q_label)
					var w_label = Label.new()
					w_label.text = "  %d  " % [item.weight]
					add_child(w_label)
					var v_label = Label.new()
					v_label.text = "  %d  " % [item.value]
					add_child(v_label)
					var texture = ImageTexture.new()
					texture = item.texture
					var TR = TextureRect.new()
					TR.texture = texture
					add_child(TR)
			else:
				pass
	if container_type == "watercollector":
		var obj_par = container_obj.get_parent()
		for item in loot_inventory.get_items():
			if item.name == "water_dirty" and obj_par.water_amt > 0:
				var item_button = Button.new()
				item_button.text = "   %s   " % [item.name]
				item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
				item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
				item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
				add_child(item_button)
				var q_label = Label.new()
				q_label.text = "  %d  " % (obj_par.water_amt)
				add_child(q_label)
				var w_label = Label.new()
				w_label.text = "  %d  " % [item.weight]
				add_child(w_label)
				var v_label = Label.new()
				v_label.text = "  %d  " % [item.value]
				add_child(v_label)
				var texture = ImageTexture.new()
				texture = item.texture
				var TR = TextureRect.new()
				TR.texture = texture
				add_child(TR)
			else:
				pass
	if container_type == "kitchen":
		var obj_par = container_obj.get_parent()
		for item in loot_inventory.get_items():
			if item.name == "water_dirty" or item.name == "tincan":
				if item.name == "water_dirty" and obj_par.water_amt > 0:
					var item_button = Button.new()
					item_button.text = "   %s   " % [item.name]
					item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
					item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
					item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
					add_child(item_button)
					var q_label = Label.new()
					q_label.text = "  %d  " % (obj_par.water_amt)
					add_child(q_label)
					var w_label = Label.new()
					w_label.text = "  %d  " % [item.weight]
					add_child(w_label)
					var v_label = Label.new()
					v_label.text = "  %d  " % [item.value]
					add_child(v_label)
					var texture = ImageTexture.new()
					texture = item.texture
					var TR = TextureRect.new()
					TR.texture = texture
					add_child(TR)
				if item.name == "tincan" and obj_par.tincan_amt > 0:
					var item_button = Button.new()
					item_button.text = "   %s   " % [item.name]
					item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
					item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
					item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
					add_child(item_button)
					var q_label = Label.new()
					q_label.text = "  %d  " % (obj_par.tincan_amt)
					add_child(q_label)
					var w_label = Label.new()
					w_label.text = "  %d  " % [item.weight]
					add_child(w_label)
					var v_label = Label.new()
					v_label.text = "  %d  " % [item.value]
					add_child(v_label)
					var texture = ImageTexture.new()
					texture = item.texture
					var TR = TextureRect.new()
					TR.texture = texture
					add_child(TR)
			else:
				pass

func _button_pressed(arg: String):
	if container_type == "player_storage":
		# loot from container
		loot_dict[arg] -= 1
		player.inventory.add_item(arg, 1)
		message.text = "You transfered a %s" % arg
	if container_type == "loot_bag":
		# loot from container
		var obj_par = container_obj.get_parent()
		if arg == "coin":
			obj_par.coin_amt -= 1
			player.inventory.add_item(arg, 1)
			message.text = "You looted a %s" % arg
	if container_type == "coop":
		# loot from container
		var obj_par = container_obj.get_parent()
		if arg == "chicken":
			obj_par.chicken_amt -= 1
			player.inventory.add_item(arg, 1)
		if arg == "egg":
			obj_par.egg_amt -= 1
			player.inventory.add_item(arg, 1)
		message.text = "You looted a %s" % arg
	if container_type == "watercollector":
		# loot from container
		var obj_par = container_obj.get_parent()
		if arg == "water_dirty":
			obj_par.water_amt -= 1
			player.inventory.add_item(arg, 1)
			message.text = "You looted a %s" % arg
	if container_type == "kitchen":
		# loot from container
		var obj_par = container_obj.get_parent()
		if arg == "water_dirty":
			obj_par.water_amt -= 1
			player.inventory.add_item(arg, 1)
		if arg == "tincan":
			obj_par.tincan_amt -= 1
			player.inventory.add_item(arg, 1)
		message.text = "You looted a %s" % arg
	_lootinv()

func _on_btn_mouse_entered(arg: String):
	# show item info on the right side
	preview_label.text = arg
	preview_label.visible = true
func _on_btn_mouse_exited():
	# hide item info on the right side
	preview_label.visible = false

func _on_exit_button_pressed():
	if container_type == "player_storage":
		# get and play close chest ani backwards
		var t_ani = container_obj.get_parent().get_node("AnimationPlayer")
		t_ani.play_backwards("half_open", -1)
	# close
	looting_ui.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _on_button_2_pressed():
	if container_type == "player_storage":
		# take all
		for item in loot_inventory.get_items():
			if loot_dict[item.name] > 0:
				player.inventory.add_item(item.name, loot_dict[item.name])
				loot_dict[item.name] = 0
		# get and play close chest ani backwards
		var t_ani = container_obj.get_parent().get_node("AnimationPlayer")
		t_ani.play_backwards("half_open", -1)
		# close ui
		looting_ui.visible = false
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	if container_type == "loot_bag":
		# take all
		for item in loot_inventory.get_items():
			if item.name == "coin":
				player.inventory.add_item(item.name, loot_dict[item.name])
				loot_dict[item.name] = 0
		# close ui
		looting_ui.visible = false
		# remove loot_bag
		var obj_par = container_obj.get_parent()
		obj_par.queue_free()
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	_lootinv()

func _on_grid_container_container_update():
	_lootinv()

func set_container_type(ctype):
	container_obj = ctype
	container_type = ctype.name
	_lootinv()

