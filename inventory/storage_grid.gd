extends GridContainer

# storage_grid.gd
# the right side panel of the LootingUI (player inventory)

@onready var preview_label := $"../../ScrollContainer2/VBoxContainer/Label"
@onready var player := $"../../../../../../../Player"
@onready var container := $"../../../../../../StorageContainer/Panel/ScrollContainer/GridContainer"

@onready var message := get_node("/root/MainScene/Hud/Messages")

signal hud_update()
signal container_update()

var loot_inventory = load("res://inventory/loot_inventory.tres")

func _ready():
	player.inventory.connect("items_changed", _testinv)
	_testinv()

func _testinv():
	# remove_at existing items from grid
	for n in get_children():
		n.queue_free()
	
	# labels for grid
	var name_label = Label.new()
	name_label.text = "    ItemName     "
	add_child(name_label)
	var amount_label = Label.new()
	amount_label.text = "- amt"
	add_child(amount_label)
	var weight_label = Label.new()
	weight_label.text = "- wgt"
	add_child(weight_label)
	var value_label = Label.new()
	value_label.text = "- val"
	add_child(value_label)
	# should add a category to sort by
	var img_label = Label.new()
	img_label.text = " "
	add_child(img_label)

	# Inventory items
	for item in player.inventory.get_items():
		# we only want one type of items
#		if item.item_reference.type == selected_type:
		var item_button = Button.new()
		item_button.text = "   %s   " % [item.name]
		item_button.connect("pressed",Callable(self,"_button_pressed").bind(item.name))
		item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(item.description))
		item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
#		item_button.minimum_size.x = 180
		add_child(item_button)
		var q_label = Label.new()
		q_label.text = "  %d  " % [item.amount]
		add_child(q_label)
		var w_label = Label.new()
		w_label.text = "  %d  " % [item.weight]
		add_child(w_label)
		var v_label = Label.new()
		v_label.text = "  %d  " % [item.value]
		add_child(v_label)
		# should add a category to sort by
		var texture = ImageTexture.new()
		texture = item.texture
		var TR = TextureRect.new()
		TR.texture = texture
		add_child(TR)

func _button_pressed(arg: String):
	# is it player storage?
	if container.container_obj.name == "player_storage":
		# transfer item to player_storage
		player.inventory.remove_item(arg, 1)
		container.loot_dict[arg] += 1
		emit_signal("hud_update")
		emit_signal("container_update")
	if container.container_obj.name == "loot_bag":
		# transfer to loot container
		var obj_par = container.container_obj.get_parent()
		if arg == "coin":
			player.inventory.remove_item(arg, 1)
			obj_par.coin_amt += 1
			emit_signal("hud_update")
			emit_signal("container_update")
		else:
			message.text = "This container only holds coin"
	if container.container_obj.name == "coop":
		# transfer to chicken coop
		var obj_par = container.container_obj.get_parent()
		if arg == "chicken":
			player.inventory.remove_item(arg, 1)
			obj_par.chicken_amt += 1
			emit_signal("hud_update")
			emit_signal("container_update")
		else:
			message.text = "This container only holds chicken"
	if container.container_obj.name == "watercollector":
		message.text = "This is not a storage container"
	if container.container_obj.name == "kitchen":
		message.text = "This is not a storage container"

func _on_btn_mouse_entered(arg: String):
	# show item info on the right side
	preview_label.text = arg
	preview_label.visible = true
func _on_btn_mouse_exited():
	# hide item info on the right side
	preview_label.visible = false

func update_inventory_display_org():
	for item_index in player.inventory.items.size():
		update_item(item_index)

func update_item(item_index):
	var inventorySlotDisplay = get_child(item_index)
	var item = player.inventory.items[item_index]
	inventorySlotDisplay.display_item(item)
#	if item is Item:
#		tex1.texture = item.texture
		#itemAmountLabel.text = str(item.amount)
#	else:
#		tex1.texture = load("res://items/empty.png")
		#itemAmountLabel.text = ""

func _on_items_changed(indexes):
	for item_index in indexes:
		update_item(item_index)

