extends AudioStreamPlayer3D

func play_sound(sound_stream):
	set_stream(sound_stream)
	play()

func _on_finished():
	queue_free()
