extends Control

@onready var time_label := $Label
@onready var sun := $"../DirectionalLight3D"

signal minute_passed()

var g_min = 1
var g_min_new = false
var g_hour = 12
var g_day = 1
var timer = 0.0
var time_multiplier = 0.5
var sun_up = true

func _process(delta):
	timer += delta * time_multiplier
	if timer >= 1.0:
		timer -= 1.0
		g_min += 1
		g_min_new = true
		if g_min == 60:
			g_min = 1
			g_hour += 1
			if g_hour == 24:
				g_hour = 0
				g_day += 1
	if g_min_new:
		var _min = "%0*d" % [2, g_min]
		var _hour = "%0*d" % [2, g_hour]
		time_label.set_text(str("Time: ", _hour , ":" , _min , " / day " , g_day ))
		emit_signal("minute_passed")

#		if sun_up and g_hour < 19:
#			sun.rotation.x += deg_to_rad(.22)
#		else:
#			sun_up = false
#		if g_hour == 6 and g_min == 1:
#			sun_up = true
#			sun.rotation.x = deg_to_rad(0)
		g_min_new = false
