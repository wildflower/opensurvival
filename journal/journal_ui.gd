extends GridContainer

# name : 
@export var j_entry: Array[String]
# text : a short description
@export var j_text: Array[String]
# progress : 0 = not started / 1 = started / 2 = done / 3 = failed
@export var j_step: Array[int]

@export var cur_quest: String

@onready var preview_label := $"../../ScrollContainer2/VBoxContainer/Label2"
@onready var message := $"../../../../../../../Hud/Messages"
@onready var player := $"../../../../../../../Player"

@onready var active_q := $"../../../../../../../Hud/ActiveQuest"

func _ready():
	update_ui()

func update_ui():
	# remove_at existing items from grid
	for n in get_children():
		n.queue_free()
	# labels for grid
	var name_label = Label.new()
	name_label.text = "    Quest name     "
	add_child(name_label)
	var sp_label = Label.new()
	sp_label.text = "   status   "
	add_child(sp_label)
	# recipe button and image
	for entry in j_entry.size():
		var item_button = Button.new()
		item_button.text = "   %s   " % j_entry[entry]
		item_button.connect("pressed",Callable(self,"_button_pressed").bind(j_entry[entry]))
		item_button.connect("mouse_entered",Callable(self,"_on_btn_mouse_entered").bind(j_text[entry]))
		item_button.connect("mouse_exited",Callable(self,"_on_btn_mouse_exited"))
		add_child(item_button)
		var step_label = Label.new()
		if j_step[entry] == 0:
			step_label.text = "not started"
		if j_step[entry] == 1:
			step_label.text = "active"
			# add active quest to hud (top right corner)
			active_q.text = j_entry[entry]
		if j_step[entry] == 2:
			step_label.text = "done"
			item_button.disabled = true
			# add active quest to hud (top right corner)
			active_q.text = "Job complete, return for your reward"
		if j_step[entry] == 3:
			step_label.text = "failed"
			item_button.disabled = true
		add_child(step_label)
		# add active quest to hud (top right corner)
		#active_q.text = j_entry[entry]

func _on_btn_mouse_entered(extra_arg_0: String):
	# show item info on the right side
	preview_label.text = extra_arg_0
	preview_label.visible = true
func _on_btn_mouse_exited():
	preview_label.visible = false

#func _button_pressed():
#	# using a button to mouseover. doesnt do anything
#	print("You clicked the journal button, it doesnt do anything")
