extends Resource

# quest.gd

class_name Quest

# quest name
@export var name: String = ""
# quest image
@export var texture: Texture
# Where are we in the quest
@export var step: int
# Description
@export var description: String = ""
# If quest require an item and how many
@export var item_name: String = ""
@export var item_quantity: int
# Reward for quests
@export var reward_name: String = ""
@export var reward_quantity: int
