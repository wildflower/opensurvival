extends GridContainer

# bountyboard_ui.gd

@export var _current_quest: Quest

@onready var bb_ui := $"../.."
@onready var hud = $"../../../Hud"

@onready var test_label = $"../Label2"

@onready var journal_ui := $"../../../PlayerUI/TabContainer/Journal/Panel/HBoxContainer/ScrollContainer/GridContainer"

@onready var message := get_node("/root/MainScene/Hud/Messages")

func _ready():
#	_avatar.texture = _current_dialog.avatar_texture
	GameEvents.connect("bountyboard_open", _bountyboard_open)

func _bountyboard_open(_cur_npc) -> void:
	hud.visible = false
	
	#_current_quest = load("res://journal/quests/%s.tres" % cur_quest)
	_current_quest = load("res://journal/quests/ClearGasstation.tres")
	
	test_label.text = _current_quest.description
	
	bb_ui.visible = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
#	if  game_pause:
#		get_tree().paused = true
#		game_pause = false
#		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
#		dialog_ui.visible = true
#	else:
#		get_tree().paused = false
#		game_pause = true
#		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
#		dialog_ui.visible = false
	
	# remove_at existing items from grid
	for n in get_children():
		n.queue_free()
	# quest layout
	var texture = ImageTexture.new()
	texture = _current_quest.texture
	var TR = TextureRect.new()
	TR.texture = texture
	add_child(TR)
	#var d_label = Label.new()
	#d_label.text = _current_quest.description
	#add_child(d_label)
	var rn_label = Label.new()
	rn_label.text = "your reward is:"
	add_child(rn_label)
	var rq_label = Label.new()
	rq_label.text = "%d coin" % [_current_quest.reward_quantity]
	add_child(rq_label)
	var accept_button = Button.new()
	accept_button.text = "Accept"
	accept_button.connect("pressed",Callable(self,"_acc_button_pressed").bind(_current_quest.name))
	add_child(accept_button)
	var close_button = Button.new()
	close_button.text = "Close"
	close_button.connect("pressed",Callable(self,"_close_button_pressed").bind(_current_quest.name))
	add_child(close_button)

func _acc_button_pressed(extra_arg_0: String):
	journal_ui.cur_quest = _current_quest.name
	# add the next journal-entry
	journal_ui.j_entry.append(_current_quest.name)
	journal_ui.j_text.append(_current_quest.description)
	journal_ui.j_step.append(1)
	journal_ui.update_ui()
	# 
	message.text = "You accepted a %s quest"  % extra_arg_0
	#
	bb_ui.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	hud.visible = true

func _close_button_pressed(_extra_arg_0: String):
	message.text = "chicken..."
	bb_ui.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	hud.visible = true
