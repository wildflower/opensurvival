## OpenSurvival

### An Open Source Survival RPG build in Godot 4

![OpenSurvival logo](./OpenSurvival_logo.png)

The game draws inspiration from games like 7DaysToDie, and I have tried to include the things I find most enjoyable in a survival RPG game, to the extend that my programming skills allow. It is far from finished, but I have made a test-map so people can try it out

All my code is GPL V.3 or later and all models, texures and sounds are CC, some with SA, BY and/or NC extension)  
The individual licences for things I did not create myself are listed under credits.  
My own models are also released in blender-format at OpenGameArt.

I'm a bit ambivalent regarding licencing, on one hand I don't see why someone should make money on work I give away for free, but... I'm not making it for the money, I'm making it because I think its fun and because I'd like to contribute to open-source in some minor way, and if I already know that I'm not going to make money of this, why should I care if someone else is?  
For now, I'll keep the project GPL, but I'm becoming more open to something like MIT or perhaps another more permissible licenses.

## Credits

I'm curently (27/01/23) using the Beta15 release of Godot 4, I use Blender for modeling, Gimp/InsaneBump for textures and ui and Debian 11 to keep it all together.

- Lowpoly Zombie / tigertowel / CC0 / www.blendswap.com/blends/view/76443
- pine-tree / Dim / CC-BY / opengameart.org/content/dims-arboretum
- SimpleMan (trader bob) / TiZeta / CC0 / www.blendswap.com/user/TiZeta
- Plastic barrel / yughues / CC0 / opengameart.org/content/plastic-barrel-pack
- chicken / CDmir / CC0 : opengameart.org/content/chicken-animated

- Zombie breathing / Huminaatio / CC0 : freesound.org/people/Huminaatio/sounds/189281/
- error sound / LorenzoTheGreat / CC-BY / freesound.org/people/LorenzoTheGreat/sounds/417794/
- drinking and eating / Taira Komori / CC-BY : www.freesound.org/people/Taira%20Komori
- digging and mining / qubodup / CC : www.freesound.org/people/qubodup
- lock-picking / miksmusic / CC-BY : www.freesound.org/people/miksmusic
- zombie pains / VHBC / CC0 : freesound.org/people/VHBC/sounds/633193/
- despair / CC-BY : freesound.org/people/phantastonia/sounds/615020/
- dirt_collision (rubble) / CC0 : freesound.org/people/RichieMcMullen/sounds/386799/
- BearTrapSound / ThePriest909 / CC0 : freesound.org/people/ThePriest909/sounds/278203/
- dying female / AmeAngelofSin / CC-BY : freesound.org/people/AmeAngelofSin/sounds/345049/
- female zombie / MadamVicious / CC0 : freesound.org/people/MadamVicious/sounds/238644/
- coins shuffle / adgawrhbshbffsfgvsrf / CC0 : freesound.org/people/adgawrhbshbffsfgvsrf/sounds/492017/
- SFX Magic (lvl up) / renatalmar / CC0 : freesound.org/people/renatalmar/sounds/264981/
- Pills / SMuntsinger / CC0 : freesound.org/people/SMuntsinger/sounds/449606/
- Gurgling / Darsycho / CC0 : freesound.org/people/Darsycho/sounds/506716/
- winning / Podsburgh / CC0 : freesound.org/people/Podsburgh/sounds/370743/
- spin / David819 / CC0 : freesound.org/people/David819/sounds/668432/
- Horror Game Menu / Eric Matyas / CC-BY : Soundimage.org 

- FingerPaint Font / Ralph du Carrois / SIL
- paper-texture / darkwood / CC-BY : opengameart.org/content/paper
- Contains assets from ambientCG.com, licensed under CC0 1.0 Universal.
- hud-icons / CC-BY / game-icons.net
- Dim's textures / CC-BY : opengameart.org/content/dims-enviromental-and-architectural-textures
- blood hit / Sinestesia / CC0 : opengameart.org/content/animations-blood-hit-and-both-d
- Hay / RedBirdTheCardinal / CC0 : opengameart.org/content/hay-0
- kitchen texture pack / p0ss / CC-BY : opengameart.org/content/kitchen-texture-pack

## Hotkeys

WASD: movement  
Ctrl: crouch  
Space: jump  
TAB: inventory  
E: interact/pickup  
1 -> 9: Toolbelt  
ESC: game-menu  
P: screenshot  

Building:  
Q: show block selector  
LMB: rotate block  
RMB: place block  
LMB (w.tool): upgrade  

## TODO
repair tools/weapons. worn-out blunderbus increasingly likely to explode/misfire, causeing player damage.  
crafting: workstation requirement - campfire, smith, lab  
game pause?  
animate trees/grass (noise shader?)  
player crouch/sneak  
POI-buildings - smith chemestry (block size ?)  
Sleeping - tirred? (sleep until morning/sundown)  
Driving - electric golfcar (solar/wind-power) - bike  
Infections/disease - bleeding - foodpoisoning - headache  
farming  
fishing  
defend campsite/trader_compound/npc quest  
supplydrops attract zombies over time  
campfire fried_chicken  
zombie variants: tank, crawler, runner, ranged, animals  
robot helper  
player head/weapon bob  
dev-mode. disable collision / cheatcode  
night and day  
zombie animations  
repairhammer in buildmode  

