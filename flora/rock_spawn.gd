extends Marker3D

# tree_spawn.gd

@onready var rock = preload("res://flora/rock.tscn")

var pos

func _ready():
	pos = self.global_transform.origin
	await get_tree().create_timer(0.2).timeout
	place_rocks()

func place_rocks():
	var b = rock.instantiate() as Node3D
	get_tree().get_root().add_child(b)
	b.global_transform.origin = pos
	b.rotation.y = deg_to_rad(180)
