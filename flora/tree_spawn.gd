extends Marker3D

# tree_spawn.gd

@onready var firtree = preload("res://flora/firtree.tscn")

var pos

func _ready():
	# get position for tree
	pos = self.global_transform.origin
	# wait for scene to load before planting trees
	await get_tree().create_timer(0.2).timeout
	plant_trees()

func plant_trees():
	# make tree instance and add to scene
	var b = firtree.instantiate() as Node3D
	get_tree().get_root().add_child(b)
	b.global_transform.origin = pos
	# random value for scale and y-rotation
	var s_val = randf_range ( 0.8, 2.2 )
	var msh1 = b.get_node("fir_l_top")
	var msh2 = b.get_node("fir_l_stump")
	msh1.scale = Vector3(s_val, s_val, s_val)
	msh2.scale = Vector3(s_val, s_val, s_val)
	msh1.rotation.y = s_val
	msh2.rotation.y = s_val
