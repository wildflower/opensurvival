extends RayCast3D

@onready var ani := $"../AnimationPlayer"
@onready var player := get_node("/root/MainScene/Player")
@onready var message := get_node("/root/MainScene/Hud/Messages")

@onready var ui_toolbelt := get_node("/root/MainScene/Hud/Toolbelt")
@onready var tool1_select := get_node("/root/MainScene/Hud/Toolbelt/Tool1/Selected")
@onready var tool2_select := get_node("/root/MainScene/Hud/Toolbelt/Tool2/Selected")
@onready var tool3_select := get_node("/root/MainScene/Hud/Toolbelt/Tool3/Selected")
@onready var tool4_select := get_node("/root/MainScene/Hud/Toolbelt/Tool4/Selected")
@onready var tool5_select := get_node("/root/MainScene/Hud/Toolbelt/Tool5/Selected")
@onready var tool6_select := get_node("/root/MainScene/Hud/Toolbelt/Tool6/Selected")
@onready var tool7_select := get_node("/root/MainScene/Hud/Toolbelt/Tool7/Selected")
@onready var tool8_select := get_node("/root/MainScene/Hud/Toolbelt/Tool8/Selected")
@onready var tool9_select := get_node("/root/MainScene/Hud/Toolbelt/Tool9/Selected")
@onready var tool0_select := get_node("/root/MainScene/Hud/Toolbelt/Tool0/Selected")

@onready var handbone := $"../Armature/Skeleton3D/BoneAttachment3D"
@onready var weapon := $"../Armature/Skeleton3D/BoneAttachment3D/MeshInstance3D"

var blood = preload("res://ui/blood.tscn")

var wood_mat = preload("res://player/building/wood_mat.tres")

var audio_player = preload("res://sound/audio_player.tscn")
var audio_build = preload("res://sound/axe_chop.wav")
var audio_tree = preload("res://sound/tree_fall.wav")
var audio_breath = preload("res://sound/outofbreath.wav")
var audio_rock = preload("res://sound/prospecting.wav")
var audio_rubble = preload("res://sound/rubble.wav")
var audio_pain = preload("res://sound/zombie_pain.wav")

@export var toolbelt = "fist"

@export var tool_1 = "fist"
@export var tool_2 = "fist"
@export var tool_3 = "fist"
@export var tool_4 = "fist"
@export var tool_5 = "fist"
@export var tool_6 = "fist"
@export var tool_7 = "fist"
@export var tool_8 = "fist"
@export var tool_9 = "fist"
@export var tool_0 = "fist"

signal hud_update()


func _ready():
	# a quick play to make sure we have the right animation
	ani.play("sword_attack", -1, 10.0, false)

func _unhandled_input(event: InputEvent) -> void:
	# tool/weapon
	if toolbelt == "stoneaxe" or toolbelt == "pickaxe" or toolbelt == "club":
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			if event is InputEventMouseButton:
				if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
					if player.stamina > 10:
						ani.play("sword_attack", -1, 2.0, false)
						player.stamina -= 10
						emit_signal("hud_update")
						if is_colliding():
							if get_collider() is CharacterBody3D and toolbelt == "club":
								var obj = get_collider()
								if obj.is_in_group("zombie"):
									obj.health -= 50
									var sound = audio_player.instantiate()
									obj.add_child(sound)
									sound.play_sound(audio_pain)
									# blod splatter
									var pos = obj.global_transform.origin
									pos.y += 1.7
									var b = blood.instantiate() as Sprite3D
									get_tree().get_root().add_child(b)
									b.global_transform.origin = pos
									await get_tree().create_timer(0.5).timeout
									b.queue_free()
							if get_collider() is StaticBody3D:
								var obj = get_collider()
								if obj.is_in_group("tree") and toolbelt == "stoneaxe":
									if obj.is_in_group("wood"):
										if obj.health < 1:
											obj.remove_from_group("tree")
											var sound = audio_player.instantiate()
											player.add_child(sound)
											sound.play_sound(audio_build)
											message.text = "You destroyed a block"
											player.inventory.add_item("log", 1)
											obj.queue_free()
										else:
											var sound = audio_player.instantiate()
											obj.add_child(sound)
											sound.play_sound(audio_build)
											obj.health -= 50
									else:
										var tree = obj.get_parent()
										if tree.health < 1:
											obj.remove_from_group("tree")
											#play_sound(audio_build, 0.5, 0.4)
											var sound = audio_player.instantiate()
											obj.add_child(sound)
											sound.play_sound(audio_build)
											var t_ani = tree.get_node("AnimationPlayer")
											t_ani.play("fir_l_topAction", -1 , 1.0, false)
											message.text = "You found 3 logs and 10 sticks"
											player.inventory.add_item("stick", 10)
											player.inventory.add_item("log", 3)
											#play_sound(audio_tree, 0.5, 2.0)
											var soundx = audio_player.instantiate()
											obj.add_child(soundx)
											soundx.play_sound(audio_tree)
											# remove after X sec.
											await get_tree().create_timer(3).timeout
											tree.queue_free()
											pass
										else:
											#play_sound(audio_build, 0.5, 0.4)
											var sound = audio_player.instantiate()
											obj.add_child(sound)
											sound.play_sound(audio_build)
											tree.health -= 1
								else:
									if obj.is_in_group("rock") and toolbelt == "pickaxe":
										var rock = obj.get_parent()
										if rock.health < 1:
											obj.remove_from_group("rock")
											#play_sound(audio_rock, 0.5, 0.4)
											var sound = audio_player.instantiate()
											obj.add_child(sound)
											sound.play_sound(audio_rock)
											var rock1 = rock.get_node("rock1")
											var r1_ani = rock1.get_node("AnimationPlayer")
											var rock2 = rock.get_node("rock2")
											var r2_ani = rock2.get_node("AnimationPlayer")
											var rock3 = rock.get_node("rock3")
											var r3_ani = rock3.get_node("AnimationPlayer")
											r1_ani.play("rock1_ani", -1 , 2.0, false)
											r2_ani.play("rock2_ani", -1 , 2.0, false)
											r3_ani.play("rock3_ani", -1 , 2.0, false)
											message.text = "You harvested 10 stone"
											player.inventory.add_item("stone", 10)
											player.inventory.add_item("ore_chunk", 3)
											#play_sound(audio_rubble, 0.5, 0.8)
											var soundx = audio_player.instantiate()
											obj.add_child(soundx)
											soundx.play_sound(audio_rubble)
											# remove after X sec.
											await get_tree().create_timer(3).timeout
											rock.queue_free()
										else:
											#play_sound(audio_rock, 0.5, 0.4)
											var sound = audio_player.instantiate()
											obj.add_child(sound)
											sound.play_sound(audio_rock)
											rock.health -= 1
					else:
						message.text = "Not enough stamina"
						#play_sound(audio_breath, 0.5, 0.4)
						var sound = audio_player.instantiate()
						player.add_child(sound)
						sound.play_sound(audio_breath)

	# build-mode
	if toolbelt == "fist" or toolbelt == "frame":
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			if event is InputEventMouseButton:
				if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
					# Do player have required materials to upgrade block
					if player.inventory.get_item("log"):
						ani.play("sword_attack", -1, 4.0, false)
						# if block and holding building-tool, lock in place (remove from "pickup" group)
						if is_colliding():
							if get_collider() is StaticBody3D:
								if get_collider().is_in_group("pickup"):
									# upgrade to wood
									get_collider().get_node("MeshInstance3D").set_surface_override_material(0, wood_mat)
									#play_sound(audio_build, 0.5, 0.4)
									var sound = audio_player.instantiate()
									get_collider().add_child(sound)
									sound.play_sound(audio_build)
									message.text = "You upgraded a block to wood"
									# make it a static block
									get_collider().remove_from_group("pickup")
									get_collider().add_to_group("wood")
									get_collider().add_to_group("tree")
									player.inventory.remove_item("log", 1)
					else:
						message.text = "You need a log to upgrade this block to wood"

func update_toolbelt(tool_str):
	if tool_str == "frame":
		weapon.mesh = load("res://player/no_weapon.mesh")
	if tool_str == "fist":
		weapon.mesh = load("res://player/no_weapon.mesh")
	if tool_str == "stoneaxe":
		weapon.mesh = load("res://player/stone_axe.mesh")
	if tool_str == "pickaxe":
		weapon.mesh = load("res://player/stone_pickaxe.mesh")
	if tool_str == "club":
		weapon.mesh = load("res://player/stone_club.mesh")

func _input(_event):
	# I should realy clean up this mess, enum?
	if Input.is_key_pressed(KEY_1):
		toolbelt = tool_1
		update_toolbelt(tool_1)
		tool1_select.visible = true
		tool2_select.visible = false
		tool3_select.visible = false
		tool4_select.visible = false
		tool5_select.visible = false
		tool6_select.visible = false
		tool7_select.visible = false
		tool8_select.visible = false
		tool9_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_2):
		toolbelt = tool_2
		update_toolbelt(tool_2)
		tool2_select.visible = true
		tool1_select.visible = false
		tool3_select.visible = false
		tool4_select.visible = false
		tool5_select.visible = false
		tool6_select.visible = false
		tool7_select.visible = false
		tool8_select.visible = false
		tool9_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_3):
		toolbelt = tool_3
		update_toolbelt(tool_3)
		tool3_select.visible = true
		tool1_select.visible = false
		tool2_select.visible = false
		tool4_select.visible = false
		tool5_select.visible = false
		tool6_select.visible = false
		tool7_select.visible = false
		tool8_select.visible = false
		tool9_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_4):
		toolbelt = tool_4
		update_toolbelt(tool_4)
		tool4_select.visible = true
		tool1_select.visible = false
		tool2_select.visible = false
		tool3_select.visible = false
		tool5_select.visible = false
		tool6_select.visible = false
		tool7_select.visible = false
		tool8_select.visible = false
		tool9_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_5):
		toolbelt = tool_5
		update_toolbelt(tool_5)
		tool5_select.visible = true
		tool1_select.visible = false
		tool2_select.visible = false
		tool3_select.visible = false
		tool4_select.visible = false
		tool6_select.visible = false
		tool7_select.visible = false
		tool8_select.visible = false
		tool9_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_6):
		toolbelt = tool_6
		update_toolbelt(tool_6)
		tool6_select.visible = true
		tool1_select.visible = false
		tool2_select.visible = false
		tool3_select.visible = false
		tool4_select.visible = false
		tool5_select.visible = false
		tool7_select.visible = false
		tool8_select.visible = false
		tool9_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_7):
		toolbelt = tool_7
		update_toolbelt(tool_7)
		tool7_select.visible = true
		tool1_select.visible = false
		tool2_select.visible = false
		tool3_select.visible = false
		tool4_select.visible = false
		tool5_select.visible = false
		tool6_select.visible = false
		tool8_select.visible = false
		tool9_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_8):
		toolbelt = tool_8
		update_toolbelt(tool_8)
		tool8_select.visible = true
		tool1_select.visible = false
		tool2_select.visible = false
		tool3_select.visible = false
		tool4_select.visible = false
		tool5_select.visible = false
		tool6_select.visible = false
		tool7_select.visible = false
		tool9_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_9):
		toolbelt = tool_9
		update_toolbelt(tool_9)
		tool9_select.visible = true
		tool1_select.visible = false
		tool2_select.visible = false
		tool3_select.visible = false
		tool4_select.visible = false
		tool5_select.visible = false
		tool6_select.visible = false
		tool7_select.visible = false
		tool8_select.visible = false
		tool0_select.visible = false
	if Input.is_key_pressed(KEY_0):
		toolbelt = tool_0
		update_toolbelt(tool_0)
		tool0_select.visible = true
		tool1_select.visible = false
		tool2_select.visible = false
		tool3_select.visible = false
		tool4_select.visible = false
		tool5_select.visible = false
		tool6_select.visible = false
		tool7_select.visible = false
		tool8_select.visible = false
		tool9_select.visible = false

