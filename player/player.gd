extends CharacterBody3D

# player.gd

const SPEED = 5.0
const JUMP_VELOCITY = 4.5
const JUMP_COST = 8

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

@onready var neck := $Neck
@onready var camera := $Neck/Camera3D

@onready var trader_ui := $"../TraderUI"
@onready var bs_ui := $"../BlockSelectUI"
@onready var player_ui := $"../PlayerUI"
@onready var hud := $"../Hud"
@onready var menu := $"../GameMenu"
@onready var ui_time := $"../GameTime"

@onready var ui_health = $"../Hud/HBoxContainer/VBoxContainer/HealthProBar"
@onready var ui_stamina = $"../Hud/HBoxContainer/VBoxContainer/StaminaProBar"
@onready var ui_hunger = $"../Hud/HBoxContainer/VBoxContainer/HungerProBar"
@onready var ui_thirst = $"../Hud/HBoxContainer/VBoxContainer/ThirstProBar"

@onready var skilltree := get_node("/root/MainScene/PlayerUI/TabContainer/Skilltree/Panel/HBoxContainer")

@onready var blood_ani := $"../Hud/Blood/AnimationPlayer"
 
@export var health: int
@export var stamina: int
@export var hunger: int
@export var thirst: int

var sprint_speed = 1
var crouch_speed = 1
var sprint_multiplier = 1.5
var crouch_multiplier = 0.5

var bs_ui_vis = false
var p_ui_vis = false

var min_count = 0
var d_count = 0

var inventory = load("res://inventory/inventory.tres")

var audio_player = preload("res://sound/audio_player.tscn")
var audio_pain = preload("res://sound/pain_female.wav")

func _ready():
	inventory.add_item("frame", 10)
	inventory.add_item("note", 1)
	ui_health.value = health
	ui_stamina.value = stamina
	ui_hunger.value = hunger
	ui_thirst.value = thirst
	GameEvents.connect("player_damage", _on_player_damage)

func _process(_delta):
	d_count += 1
	if d_count > 42 and sprint_speed == 1:
		if stamina < 100:
			stamina += 1
			ui_stamina.value = stamina
		d_count = 0

func _input(_event):
	if Input.is_key_pressed(KEY_TAB):
		if trader_ui.is_visible_in_tree():
			print("dont open inventory when trading")
		else:
			if p_ui_vis:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				p_ui_vis = false
				player_ui.visible = false
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
				p_ui_vis = true
				player_ui.visible = true
	if Input.is_key_pressed(KEY_Q):
		if bs_ui_vis:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			bs_ui_vis = false
			bs_ui.visible = false
			hud.visible = true
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			bs_ui_vis = true
			bs_ui.visible = true
			hud.visible = false
	if Input.is_key_pressed(KEY_SHIFT):
		sprint_speed = sprint_multiplier
	else:
		sprint_speed = 1
#	if Input.is_key_pressed(KEY_CTRL):
#		crouch_speed = crouch_multiplier
#		sprint_speed = 1 # The control key cancel the shift key if it is pressed
#		crouch(true)
#	else:
#		crouch_speed = 1
#		crouch(false)
	if Input.is_key_pressed(KEY_P):
		var image = get_viewport().get_texture().get_image()
		var time = Time.get_datetime_string_from_system()
		print(time)
		#w_label.text = "  %d  " % [item.weight]
		#var scr_str = "res://screenshot_%d.png" % [time]
		image.save_png("res://screenshot_%s.png" % [time])

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		if trader_ui.is_visible_in_tree():
			print("dont open inventory when trading")
		else:
			# dont show game-menu when inventory is open
			if player_ui.visible:
	#			pass
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				p_ui_vis = false
				player_ui.visible = false
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
				menu.visible = true
				hud.visible = false
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if event is InputEventMouseMotion:
			neck.rotate_y(-event.relative.x * 0.01)
			camera.rotate_x(-event.relative.y * 0.01)
			camera.rotation.x = clamp(camera.rotation.x, deg_to_rad(-80), deg_to_rad(80))

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
	
	# Handle Jump.
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		if stamina >= JUMP_COST:
			velocity.y = JUMP_VELOCITY
			stamina -= JUMP_COST
			ui_stamina.value = stamina
	
	# Get the input direction and handle the movement/deceleration.
	var input_dir = Input.get_vector("left", "right", "forward", "back")
	var direction = (neck.transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED * sprint_speed
		velocity.z = direction.z * SPEED * sprint_speed
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED * sprint_speed)
		velocity.z = move_toward(velocity.z, 0, SPEED * sprint_speed)
	
	move_and_slide()

#func crouch(crouched):
#	if crouched:
#		$CollisionShape3D.shape.height = 0.5
#		$MeshInstance.mesh.mid_height = 0.5
#		$Neck.translation.y = 0.4
#	else:
#		$CollisionShape3D.shape.height = 1.0
#		$MeshInstance.mesh.mid_height = 1
#		$Neck.translation.y = 0.8

func _on_game_time_minute_passed():
	if health < 100 and skilltree.healing_skill == 1:
		health += 1
		ui_health.value = health
	min_count += 1
	if min_count >= 5:
		if hunger >= 1:
			hunger -= 1
		ui_hunger.value = hunger
		if thirst >= 1:
			thirst -= 1
		ui_thirst.value = thirst
		min_count = 0
	else:
		pass

func _on_shoot_ray_cast_hud_update():
	ui_stamina.value = stamina

func _on_grid_container_hud_update():
	ui_health.value = health
	ui_stamina.value = stamina
	ui_hunger.value = hunger
	ui_thirst.value = thirst

func _on_player_damage(damage):
	blood_ani.play("blood", -1 , 1.0, false)
	var sound = audio_player.instantiate()
	self.add_child(sound)
	sound.play_sound(audio_pain)
	health -= damage
	ui_health.value = health

