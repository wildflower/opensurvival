extends RayCast3D

# pickup.gd

@onready var player := $"../../.."

@onready var pickuplabel := get_node("/root/MainScene/Hud/CrossHair/PickupLabel")
@onready var interactlabel := get_node("/root/MainScene/Hud/CrossHair/InteractLabel")
@onready var message := get_node("/root/MainScene/Hud/Messages")
@onready var dialog := get_node("/root/MainScene/DialogUI")
@onready var journal := get_node("/root/MainScene/PlayerUI/TabContainer/Journal/Panel/HBoxContainer/ScrollContainer/GridContainer")
@onready var player_ui := get_node("/root/MainScene/PlayerUI")
@onready var bb_ui := get_node("/root/MainScene/BountyBoardUI")
@onready var looting_ui := get_node("/root/MainScene/LootingUI")
@onready var storage_container := get_node("/root/MainScene/LootingUI/StorageContainer")
@onready var grid_container := get_node("/root/MainScene/LootingUI/StorageContainer/Panel/ScrollContainer/GridContainer")

var audio_player = preload("res://sound/audio_player.tscn")
var audio_pickup = preload("res://sound/pickup.wav")
var audio_lockpick = preload("res://sound/lockpick.wav")
var audio_lockpick_break = preload("res://sound/lockpick_break.wav")
var audio_open_chest = preload("res://sound/open_chest.wav")
var audio_error = preload("res://sound/error.wav")

func _ready():
	randomize()

func _process(_delta):
	# pointing at an item
	var cursor_object = self.get_collider()
	if cursor_object == null:
		pass
		pickuplabel.visible = false
		interactlabel.visible = false
	else:
		if cursor_object.is_in_group("pickup") or cursor_object.is_in_group("item"):
			pickuplabel.visible = true
		else:
			pickuplabel.visible = false
		if cursor_object.is_in_group("npc") or cursor_object.is_in_group("loot_chest") or cursor_object.is_in_group("player_storage"):
			if cursor_object.is_in_group("loot_chest"):
				interactlabel.text = "locked (E)"
				interactlabel.visible = true
			else:
				interactlabel.text = "interact (E)"
				interactlabel.visible = true
		else:
			interactlabel.visible = false

func _input(_event):
	if Input.is_key_pressed(KEY_E):
		if is_colliding():
			if get_collider() is StaticBody3D:
				var obj = get_collider()
				if obj.is_in_group("item"):
					var i_name = str(obj.name)
					var sound = audio_player.instantiate()
					player.add_child(sound)
					sound.play_sound(audio_pickup)
					message.text = "You found a " + i_name
					player.inventory.add_item(i_name, 1)
					# temp solution to test-chicken
					# FIX ME PLEASE
					if i_name != "chicken":
						# remove everything incl. spawn-point
						var obj_par = obj.get_parent()
						obj_par.queue_free()
					else:
						obj.queue_free()
				if obj.is_in_group("pickup"):
					var sound = audio_player.instantiate()
					player.add_child(sound)
					sound.play_sound(audio_pickup)
					message.text = "You found a frame"
					player.inventory.add_item("frame", 1)
					obj.queue_free()
				if obj.is_in_group("player_storage"):
					if obj.is_in_group("player_owned"):
						var sound = audio_player.instantiate()
						obj.add_child(sound)
						sound.play_sound(audio_open_chest)
						var chest = obj.get_parent()
						var t_ani = chest.get_node("AnimationPlayer")
						t_ani.play("half_open", -1 , 1.0, false)
						Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
						grid_container.set_container_type(obj)
						looting_ui.visible = true
						var soundx = audio_player.instantiate()
						obj.add_child(soundx)
						soundx.play_sound(audio_pickup)
					else:
						Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
						grid_container.set_container_type(obj)
						looting_ui.visible = true
				if obj.is_in_group("npc"):
					var cur_npc = obj.name
					if cur_npc == "BountyBoard" or cur_npc == "slotmachine":
						if cur_npc == "BountyBoard":
							var quest = journal.cur_quest
							GameEvents.emit_signal("bountyboard_open", quest)
						else:
							if player.inventory.get_item("coin"):
								player.inventory.remove_item("coin", 1)
								var sm = obj.get_parent()
								sm.run_slot()
							else:
								message.text = "You need coins to play"
								var sound = audio_player.instantiate()
								obj.add_child(sound)
								sound.play_sound(audio_error)
					else:
						if cur_npc == "hold_1" or cur_npc == "hold_2" or cur_npc == "hold_3":
							var sm = obj.get_parent()
							sm.hold_slot(cur_npc)
						else:
							GameEvents.emit_signal("dialog_initiated", cur_npc)
				if obj.is_in_group("loot_chest"):
					# check for lockpick in inventory
					if player.inventory.get_item("lockpick"):
						var chest = obj.get_parent()
						var chest_randi = randi_range(0, 10)
						if chest_randi > 3:
							if chest_randi > 7:
								var sound = audio_player.instantiate()
								obj.add_child(sound)
								sound.play_sound(audio_lockpick_break)
								player.inventory.remove_item("lockpick", 1)
								message.text = "You broke a lockpick"
							else:
								var sound = audio_player.instantiate()
								obj.add_child(sound)
								sound.play_sound(audio_lockpick)
								message.text = "Your lockpick slipped"
						else:
							obj.remove_from_group("loot_chest")
							var sound = audio_player.instantiate()
							obj.add_child(sound)
							sound.play_sound(audio_lockpick)
							await get_tree().create_timer(0.3).timeout
							var soundx = audio_player.instantiate()
							obj.add_child(soundx)
							soundx.play_sound(audio_open_chest)
							var t_ani = chest.get_node("AnimationPlayer")
							t_ani.play("open_close", -1 , 1.0, false)
							message.text = "You opened a chest"
					else:
						message.text = "You need a lockpick to open this chest"
						var sound = audio_player.instantiate()
						obj.add_child(sound)
						sound.play_sound(audio_error)
				else:
					pass

