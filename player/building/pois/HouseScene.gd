extends Node3D

# A basic house scene build with un-upgraded blocks

@onready var square = preload("res://player/building/square.tscn")
@onready var square_half = preload("res://player/building/square_half.tscn")
@onready var roof_tip = preload("res://player/building/roof_tip.tscn")
@onready var roof_top = preload("res://player/building/roof_top.tscn")
@onready var roof_tip_half = preload("res://player/building/roof_tip_half.tscn")
@onready var roof_top_half = preload("res://player/building/roof_top_half.tscn")
@onready var stair = preload("res://player/building/stair.tscn")
@onready var stair_trim = preload("res://player/building/stair_trim.tscn")
@onready var wall = preload("res://player/building/wall.tscn")
@onready var wall_corner = preload("res://player/building/wall_corner.tscn")

var cur_block = "square_sprite"

var cur_rotation = 0

var pos = Vector3(0,0,0)

var center_pos = Vector3(0,0,0)


func _input(_event):
	if Input.is_key_pressed(KEY_H):
		#_build_house()
		getallnodes(self)

func _build_house():
	self.top_level = true
	pos = center_pos.global_transform.origin
	# base level
	for _i in self.get_children():
		print(_i.global_transform.origin)
	
#	for i in range(0, floor.size()):
		#print("Accessing item at index " + str(i))
#		print (floor[i])

func getallnodes(node):
	for N in node.get_children():
		#print(N.get_name())
		pos = N.global_transform.origin
		if N.is_in_group("cube"):
			var b = square.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
		if N.is_in_group("stair"):
			var b = stair.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			b.rotation.y = deg_to_rad(180)
		if N.is_in_group("wall"):
			var b = wall.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			#b.rotation.y = deg_to_rad(90)
		if N.is_in_group("wall90"):
			var b = wall.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			b.rotation.y = deg_to_rad(90)
		if N.is_in_group("wall180"):
			var b = wall.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			b.rotation.y = deg_to_rad(180)
		if N.is_in_group("wall270"):
			var b = wall.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			b.rotation.y = deg_to_rad(270)
		if N.is_in_group("wall_corner1"):
			var b = wall_corner.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			b.rotation.y = deg_to_rad(270)
		if N.is_in_group("wall_corner2"):
			var b = wall_corner.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			b.rotation.y = deg_to_rad(180)
		if N.is_in_group("wall_corner3"):
			var b = wall_corner.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			#b.rotation.y = deg_to_rad(90)
		if N.is_in_group("wall_corner4"):
			var b = wall_corner.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			b.rotation.y = deg_to_rad(90)
		if N.is_in_group("roof"):
			var b = square.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			
			
#		if N.get_child_count() > 0:
#			print("["+N.get_name()+"]")
#			getallnodes(N)
#		else:
#			# Do something
#			print(N.get_name())

func traverse_nested_array(arr):
	var l_count = 0
	for i in range(arr.size()):
		for i2 in range(arr[i].size()):
			print(arr[i][i2])
			if l_count == 5:
				pos.x -= 5
				pos.z += 1
			var b = square.instantiate() as Node3D
			get_tree().get_root().add_child(b)
			b.global_transform.origin = pos
			pos.x += 1
			l_count += 1
		
		
#	for i in range(5):
#		var b = square.instantiate() as Node3D
#		get_tree().get_root().add_child(b)
#		b.global_transform.origin = pos
#		pos.x += 1
#	pos.z += 1
#	pos.x -= 1
#	for i in range(5):
#		var b = square.instantiate() as Node3D
#		get_tree().get_root().add_child(b)
#		b.global_transform.origin = pos
#		pos.x -= 1
#	pos.z += 1
#	pos.x += 1
#	for i in range(5):
#		var b = square.instantiate() as Node3D
#		get_tree().get_root().add_child(b)
#		b.global_transform.origin = pos
#		pos.x += 1
#	pos.z += 1
#	pos.x -= 1
#	for i in range(5):
#		var b = square.instantiate() as Node3D
#		get_tree().get_root().add_child(b)
#		b.global_transform.origin = pos
#		pos.x -= 1
#	# wall_corners
#	pos = center_pos.global_transform.origin
#	pos.y += 1
#	for i in range(3):
#		var b = wall_corner.instantiate() as Node3D
#		get_tree().get_root().add_child(b)
#		b.global_transform.origin = pos
#		b.rotation.y = deg_to_rad(-90)
#		pos.y += 1
#	pos.y -= 3
#	pos.x += 4
#	for i in range(3):
#		var b = wall_corner.instantiate() as Node3D
#		get_tree().get_root().add_child(b)
#		b.global_transform.origin = pos
#		b.rotation.y = deg_to_rad(180)
#		pos.y += 1
#	
#	b.global_transform.origin = snap
#	b.rotation.y = deg_to_rad(cur_rotation)
#	snap.x = snapped(raycast.get_collision_point().x + raycast.get_collision_normal().x / 10, 1)
#	snap.y = snapped(raycast.get_collision_point().y + raycast.get_collision_normal().y / 10, 1)
#	snap.z = snapped(raycast.get_collision_point().z + raycast.get_collision_normal().z / 10, 1)
