extends Control

# /player/building/block_select.gd

@onready var message := get_node("/root/MainScene/Hud/Messages")

func _on_square_but_pressed():
	BlockData.cur_block = "square_sprite"
	BlockData.emit_signal("block_changed", "square_sprite")
	message.text = "cube selected"

func _on_square_half_but_pressed():
	BlockData.cur_block = "square_half_sprite"
	BlockData.emit_signal("block_changed", "square_half_sprite")
	message.text = "cube_half selected"

func _on_stair_but_pressed():
	BlockData.cur_block = "stair_sprite"
	BlockData.emit_signal("block_changed", "stair_sprite")
	message.text = "stair selected"

func _on_stair_trim_but_pressed():
	BlockData.cur_block = "stair_trim_sprite"
	BlockData.emit_signal("block_changed", "stair_trim_sprite")
	message.text = "stair_trim selected"

func _on_wall_but_pressed():
	BlockData.cur_block = "wall_sprite"
	BlockData.emit_signal("block_changed", "wall_sprite")
	message.text = "wall selected"

func _on_wall_corner_but_pressed():
	BlockData.cur_block = "wall_corner_sprite"
	BlockData.emit_signal("block_changed", "wall_corner_sprite")
	message.text = "wall_corner selected"

func _on_roof_tip_but_pressed():
	BlockData.cur_block = "roof_tip_sprite"
	BlockData.emit_signal("block_changed", "roof_tip_sprite")
	message.text = "roof_tip selected"

func _on_roof_tip_half_but_pressed():
	BlockData.cur_block = "roof_tip_half_sprite"
	BlockData.emit_signal("block_changed", "roof_tip_half_sprite")
	message.text = "roof_tip_half selected"

func _on_roof_top_but_pressed():
	BlockData.cur_block = "roof_top_sprite"
	BlockData.emit_signal("block_changed", "roof_top_sprite")
	message.text = "roof_top selected"

func _on_roof_top_half_but_pressed():
	BlockData.cur_block = "roof_top_half_sprite"
	BlockData.emit_signal("block_changed", "roof_top_half_sprite")
	message.text = "roof_top_half selected"

