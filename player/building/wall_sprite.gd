extends Node3D

# /player/building/wall_sprite.gd

@onready var raycast = get_parent()
@onready var raycast_owner = get_parent().get_owner()

@onready var square = preload("res://player/building/square.tscn")
@onready var square_half = preload("res://player/building/square_half.tscn")
@onready var roof_tip = preload("res://player/building/roof_tip.tscn")
@onready var roof_top = preload("res://player/building/roof_top.tscn")
@onready var roof_tip_half = preload("res://player/building/roof_tip_half.tscn")
@onready var roof_top_half = preload("res://player/building/roof_top_half.tscn")
@onready var stair = preload("res://player/building/stair.tscn")
@onready var stair_trim = preload("res://player/building/stair_trim.tscn")
@onready var wall = preload("res://player/building/wall.tscn")
@onready var wall_corner = preload("res://player/building/wall_corner.tscn")

var audio_player = preload("res://sound/audio_player.tscn")
var error = preload("res://sound/error.wav")

var cur_block = "square_sprite"

var cur_rotation = 0

var snap = Vector3()

func _ready():
	BlockData.block_changed.connect(_on_block_changed)
	cur_block = BlockData.cur_block
	cur_rotation = BlockData.cur_rotation
	self.top_level = true
	self.global_transform.origin = raycast.get_collision_point()
	snap.x = snapped(raycast.get_collision_point().x + raycast.get_collision_normal().x / 10, 1)
	snap.y = snapped(raycast.get_collision_point().y + raycast.get_collision_normal().y / 10, 1)
	snap.z = snapped(raycast.get_collision_point().z + raycast.get_collision_normal().z / 10, 1)
	self.global_transform.origin = snap

func _input(_event):
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if Input.is_action_just_pressed("fire"):
			if cur_rotation < 270:
				cur_rotation = cur_rotation + 90
			else:
				cur_rotation = 0
			BlockData.cur_rotation = cur_rotation
		if _event is InputEventMouseButton:
			if _event.button_index == MOUSE_BUTTON_RIGHT and _event.pressed:
				#if ! raycast_owner have frame in inventory:
				var item = raycast_owner.inventory.get_item("frame")
				if not item:
					#play_sound(error, 0.5, 0.4)
					var sound = audio_player.instantiate()
					self.add_child(sound)
					sound.play_sound(error)
					return
				var b = square.instantiate() as Node3D
				if cur_block == "square_half_sprite":
					b = square_half.instantiate() as Node3D
				if cur_block == "roof_tip_sprite":
					b = roof_tip.instantiate() as Node3D
				if cur_block == "roof_top_sprite":
					b = roof_top.instantiate() as Node3D
				if cur_block == "roof_tip_half_sprite":
					b = roof_tip_half.instantiate() as Node3D
				if cur_block == "roof_top_half_sprite":
					b = roof_top_half.instantiate() as Node3D
				if cur_block == "stair_sprite":
					b = stair.instantiate() as Node3D
				if cur_block == "stair_trim_sprite":
					b = stair_trim.instantiate() as Node3D
				if cur_block == "wall_sprite":
					b = wall.instantiate() as Node3D
				if cur_block == "wall_corner_sprite":
					b = wall_corner.instantiate() as Node3D
				#w.golbal_transform.origin = self.global_transform.origin
				get_tree().get_root().add_child(b)
				raycast_owner.inventory.remove_item("frame", 1)
				snap.x = snapped(raycast.get_collision_point().x + raycast.get_collision_normal().x / 10, 1)
				snap.y = snapped(raycast.get_collision_point().y + raycast.get_collision_normal().y / 10, 1)
				snap.z = snapped(raycast.get_collision_point().z + raycast.get_collision_normal().z / 10, 1)
				b.global_transform.origin = snap
				b.rotation.y = deg_to_rad(cur_rotation)
				#self.queue_free()

func _process(_delta):
	self.global_transform.origin = raycast.get_collision_point()
	snap.x = snapped(raycast.get_collision_point().x + raycast.get_collision_normal().x / 10, 1)
	snap.y = snapped(raycast.get_collision_point().y + raycast.get_collision_normal().y / 10, 1)
	snap.z = snapped(raycast.get_collision_point().z + raycast.get_collision_normal().z / 10, 1)
	self.global_transform.origin = snap
	self.rotation = raycast_owner.rotation
	self.rotation.y = deg_to_rad(cur_rotation)

func _on_block_changed(block):
#	cur_block = BlockData.cur_block
	cur_block = block

