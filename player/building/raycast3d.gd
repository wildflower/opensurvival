extends RayCast3D

# /player/building/raycast3d.gd

@onready var block_sprite = preload("res://player/building/square_sprite.tscn")

@onready var square_sprite = preload("res://player/building/square_sprite.tscn")
@onready var square_half_sprite = preload("res://player/building/square_half_sprite.tscn")
@onready var roof_tip_sprite = preload("res://player/building/roof_tip_sprite.tscn")
@onready var roof_top_sprite = preload("res://player/building/roof_top_sprite.tscn")
@onready var roof_tip_half_sprite = preload("res://player/building/roof_tip_half_sprite.tscn")
@onready var roof_top_half_sprite = preload("res://player/building/roof_top_half_sprite.tscn")
@onready var stair_sprite = preload("res://player/building/stair_sprite.tscn")
@onready var stair_trim_sprite = preload("res://player/building/stair_trim_sprite.tscn")
@onready var wall_sprite = preload("res://player/building/wall_sprite.tscn")
@onready var wall_corner_sprite = preload("res://player/building/wall_corner_sprite.tscn")


var cur_block = "square_sprite"

func _ready():
	BlockData.block_changed.connect(_on_block_changed)

func _on_block_changed(block):
#	cur_block = BlockData.cur_block
	cur_block = block
	if cur_block == "square_sprite":
		block_sprite = square_sprite
		update_block()
	if cur_block == "square_half_sprite":
		block_sprite = square_half_sprite
		update_block()
	if cur_block == "roof_tip_sprite":
		block_sprite = roof_tip_sprite
		update_block()
	if cur_block == "roof_top_sprite":
		block_sprite = roof_top_sprite
		update_block()
	if cur_block == "roof_tip_half_sprite":
		block_sprite = roof_tip_half_sprite
		update_block()
	if cur_block == "roof_top_half_sprite":
		block_sprite = roof_top_half_sprite
		update_block()
	if cur_block == "stair_sprite":
		block_sprite = stair_sprite
		update_block()
	if cur_block == "stair_trim_sprite":
		block_sprite = stair_trim_sprite
		update_block()
	if cur_block == "wall_sprite":
		block_sprite = wall_sprite
		update_block()
	if cur_block == "wall_corner_sprite":
		block_sprite = wall_corner_sprite
		update_block()

func _input(_event):
	if Input.is_key_pressed(KEY_1):
		if self.get_child_count() == 0:
			var b = block_sprite.instantiate()
			add_child(b)
		else:
			get_child(0).queue_free()

func update_block():
	if self.get_child_count() == 0:
		var b = block_sprite.instantiate()
		add_child(b)
	else:
		get_child(0).queue_free()
		var b = block_sprite.instantiate()
		add_child(b)
