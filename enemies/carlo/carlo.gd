extends CharacterBody3D

# /enemies/carlo/carlo.gd

@onready var ani := $"AnimationPlayer"
@onready var ray := $RayCast3D
@onready var skel := $metarig/Skeleton3D
@onready var coll := $CollisionShape3D
@onready var rig := $metarig
@onready var as_player := $"./AudioStreamPlayer3D"

@onready var skilltree := get_node("/root/MainScene/PlayerUI/TabContainer/Skilltree/Panel/HBoxContainer")

@onready var player := get_node("/root/MainScene/Player")

@onready var loot_bag = preload("res://entities/loot_bag/loot_bag.tscn")

@export var health: int

var audio_player = preload("res://sound/audio_player.tscn")
var audio_pain = preload("res://sound/pain_female.wav")

var timer = Timer.new()

const SPEED = 0.4
const JUMP_VELOCITY = 4.5

var track = false
var fight = false
var death = false
var idle = true

var can_dmg = true
var loot_drop = true

func _ready():
	timer.connect("timeout", timer_timeout)
	timer.wait_time = 1
	timer.one_shot = true
	add_child(timer)

func timer_timeout():
	can_dmg = true

#func _process(_delta):

func _physics_process(delta):
	var p_pos_x = player.global_position.x
	var p_pos_z = player.global_position.z
	
	if health == 0:
		if !death:
			skilltree.experience += 200
			skilltree.update_exp()
		death = true
		ani.play("zombie_death", -1 , 1.0, false)
		await ani.animation_finished
		coll.disabled = true
		skel.physical_bones_start_simulation()
		as_player.stop()
		health = 1
		if loot_drop:
			loot_drop = false
			await get_tree().create_timer(3).timeout
			var pos = self.global_transform.origin
			var lb = loot_bag.instantiate()
			get_tree().get_root().add_child(lb)
			lb.global_transform.origin = pos
			self.queue_free()
	if death:
		fight = false
		track = false
		idle = false
	# move towards player
	if track:
		# look at player
		look_at(Vector3(p_pos_x, global_position.y, p_pos_z), Vector3.UP)
		ani.play("zombie_waddle", -1 , 1.0, false)
		var dir = (player.global_position - global_position).normalized()
		move_and_collide(dir * SPEED * delta)
	# fight player
	if fight:
		# look at player
		look_at(Vector3(p_pos_x, global_position.y, p_pos_z), Vector3.UP)
		# try to hit player
		ani.play("zombie_fight", -1 , 1.0, false)
		await ani.animation_finished
		fight = false
		track = true
	if idle:
		ani.play("zombie_idle", -1 , 1.0, false)

func _on_area_3d_body_entered(body):
	if body.name == "Player":
		if ray.is_colliding():
			var obj = ray.get_collider()
			if can_dmg and obj.name == "Player":
				var sound = audio_player.instantiate()
				obj.add_child(sound)
				sound.play_sound(audio_pain)
				GameEvents.emit_signal("player_damage", 10)
				timer.start()
				can_dmg = false

func _on_area_hear_body_entered(body):
	if body.name == "Player":
		idle = false
		track = true

func _on_area_close_body_entered(body):
	if body.name == "Player":
		fight = true
		track = false
