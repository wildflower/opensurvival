extends Control

var self_vis = true

func _input(_event):
	if Input.is_key_pressed(KEY_F1):
		if self_vis:
			self.visible = false
			self_vis = false
		else:
			self_vis = true
			self.visible = true

